﻿using SecureMyPC.controls;
using SecureMyPC.policies;
using SecureMyPC.programs.lgpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC
{
    public partial class BaseForm : Form
    {
        public BaseForm()
        {
            InitializeComponent();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            this.moveAbleWindow1.ControlTarget = new Control[] { this.logo, this };

            FileManager f = new FileManager();
            Config cfg = f.LoadYamlConfig();

            if (cfg == null)
                cfg = new Config();

            if (cfg.WORKDIR_LOCATIONS == null)
                cfg.WORKDIR_LOCATIONS = new Dictionary<string, string>();

        }




       public bool WindowMode
        {
            get
            {
                return this.optionalControl1.Visible;
            }
            set
            {
                if(value)
                {
                    OpenOptionalControl();
                } else
                {
                    OpenMainControl();
                }
                this.Update();
            }
        }

        public void OpenMainControl()
        {
            this.mainControl1.Show();
            this.optionalControl1.Hide();

            this.progressControl1.TargetControl = this.mainControl1;
            this.mainControl1.ProgressTarget = this.progressControl1;

            this.Width= 424;
            this.Height = 642;

            this.Update();
        }

        public void OpenOptionalControl()
        {
            this.mainControl1.Hide();
            this.optionalControl1.Show();

            this.progressControl1.TargetControl = this.optionalControl1;
            this.optionalControl1.ProgressTarget = this.progressControl1;

            this.Height = 387;
            this.Width = 424;

            this.Update();

        }

        private void customButton1_ButtonClick(object sender, EventArgs e)
        {
            if(this.progressControl1.IsProgressing)
            {
                MessageBox.Show("please wait when the programs process has been finished!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.Invoke(new MethodInvoker(delegate
            {
                this.WindowMode = false;
            }));
        }

        private void customButton2_ButtonClick(object sender, EventArgs e)
        {
            if(this.progressControl1.IsProgressing)
            {
                MessageBox.Show("please wait when the programs process has been finished!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.Invoke(new MethodInvoker(delegate
            {
                this.WindowMode = true;
            }));
        }

        private void customButton23_ButtonClick(object sender, EventArgs e)
        {
            if(this.progressControl1.IsProgressing)
            {
                MessageBox.Show("please wait when the programs process has been finished!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Application.Exit();
        }
    }
}
