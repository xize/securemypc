﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC.programs.lgpo
{
    public class LGPO
    {

        public string CreateBackup(string path)
        {
            //MessageBox.Show(Application.StartupPath+@"\programs\lgpo\LGPO");
            ProcessStartInfo pinfo = new ProcessStartInfo(Application.StartupPath + @"\programs\lgpo\LGPO");
            
            if(Directory.Exists(path))
                Directory.Delete(path, true);

            Directory.CreateDirectory(path);

            pinfo.Arguments = "/b "+path;
            pinfo.UseShellExecute = false;
            pinfo.CreateNoWindow = true;

            Process p = Process.Start(pinfo);
            p.WaitForExit();
            p.Dispose();

            return Directory.EnumerateDirectories(path).FirstOrDefault();
        }

        public void LoadScript(string script)
        {
            ProcessStartInfo pinfo = new ProcessStartInfo("cmd");

            pinfo.Arguments = "/c "+ Application.StartupPath + @"\programs\lgpo\LGPO" + " /t " + script;
            pinfo.UseShellExecute = false;
            pinfo.CreateNoWindow = true;

            Process p = Process.Start(pinfo);
            p.WaitForExit();
            p.Dispose();
        }

        public void GenerateScript(string path, string scriptname)
        {
            string path2 = Path.Combine(path, @"DomainSysvol\GPO\Machine\registry.pol");

            ProcessStartInfo pinfo = new ProcessStartInfo("cmd");

            pinfo.Arguments = "/c "+ Application.StartupPath + @"\programs\lgpo\LGPO" + " /parse /m "+path2+" > "+path + scriptname+".script";
            pinfo.UseShellExecute = false;
            pinfo.UseShellExecute = false;
            pinfo.CreateNoWindow = true;
            Process proc = Process.Start(pinfo);
            proc.WaitForExit();
            proc.Dispose();

        }

    }
}
