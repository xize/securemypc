﻿
namespace SecureMyPC
{
    partial class OptionalControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionalControl));
            this.label6 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.customButton1 = new SecureMyPC.controls.CustomButton();
            this.customButton2 = new SecureMyPC.controls.CustomButton();
            this.smallDots14 = new SecureMyPC.controls.SmallDots();
            this.runasnonadmincontextbtnunapply = new SecureMyPC.controls.CustomButton();
            this.runasnonadmincontextbtnapply = new SecureMyPC.controls.CustomButton();
            this.smallDots13 = new SecureMyPC.controls.SmallDots();
            this.duckhunterundobtn = new SecureMyPC.controls.CustomButton();
            this.duckhunterbtnapply = new SecureMyPC.controls.CustomButton();
            this.smallDots12 = new SecureMyPC.controls.SmallDots();
            this.spoolerbtnunapply = new SecureMyPC.controls.CustomButton();
            this.spoolerbtnapply = new SecureMyPC.controls.CustomButton();
            this.smallDots11 = new SecureMyPC.controls.SmallDots();
            this.mbrbtnunapply = new SecureMyPC.controls.CustomButton();
            this.mbrapplybtn = new SecureMyPC.controls.CustomButton();
            this.smallDots6 = new SecureMyPC.controls.SmallDots();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(13, 29);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 12);
            this.label6.TabIndex = 31;
            this.label6.Text = "Install MBR filter";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.DimGray;
            this.label11.Location = new System.Drawing.Point(13, 69);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(109, 12);
            this.label11.TabIndex = 51;
            this.label11.Text = "Disable print spooler";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.DimGray;
            this.label14.Location = new System.Drawing.Point(15, 109);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(106, 12);
            this.label14.TabIndex = 60;
            this.label14.Text = "Install Duck hunter:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.DimGray;
            this.label15.Location = new System.Drawing.Point(13, 149);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(276, 12);
            this.label15.TabIndex = 64;
            this.label15.Text = "Add \"run as non admin\" to the rightclick context menu";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.DimGray;
            this.label13.Location = new System.Drawing.Point(13, 188);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(168, 12);
            this.label13.TabIndex = 68;
            this.label13.Text = "Install Cipher plugin for keepass";
            // 
            // customButton1
            // 
            this.customButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.customButton1.BackColor = System.Drawing.Color.Transparent;
            this.customButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customButton1.Cursor = System.Windows.Forms.Cursors.No;
            this.customButton1.Enabled2 = false;
            this.customButton1.Location = new System.Drawing.Point(358, 188);
            this.customButton1.Name = "customButton1";
            this.customButton1.Size = new System.Drawing.Size(51, 17);
            this.customButton1.TabIndex = 67;
            this.customButton1.Text2 = "Undo";
            // 
            // customButton2
            // 
            this.customButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.customButton2.BackColor = System.Drawing.Color.Transparent;
            this.customButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customButton2.Enabled2 = true;
            this.customButton2.Location = new System.Drawing.Point(298, 188);
            this.customButton2.Name = "customButton2";
            this.customButton2.Size = new System.Drawing.Size(54, 17);
            this.customButton2.TabIndex = 66;
            this.customButton2.Text2 = "Apply";
            // 
            // smallDots14
            // 
            this.smallDots14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots14.BackColor = System.Drawing.Color.Transparent;
            this.smallDots14.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots14.BackgroundImage")));
            this.smallDots14.Location = new System.Drawing.Point(13, 172);
            this.smallDots14.Name = "smallDots14";
            this.smallDots14.Size = new System.Drawing.Size(397, 4);
            this.smallDots14.TabIndex = 65;
            // 
            // runasnonadmincontextbtnunapply
            // 
            this.runasnonadmincontextbtnunapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.runasnonadmincontextbtnunapply.BackColor = System.Drawing.Color.Transparent;
            this.runasnonadmincontextbtnunapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.runasnonadmincontextbtnunapply.Cursor = System.Windows.Forms.Cursors.No;
            this.runasnonadmincontextbtnunapply.Enabled2 = false;
            this.runasnonadmincontextbtnunapply.Location = new System.Drawing.Point(358, 149);
            this.runasnonadmincontextbtnunapply.Name = "runasnonadmincontextbtnunapply";
            this.runasnonadmincontextbtnunapply.Size = new System.Drawing.Size(51, 17);
            this.runasnonadmincontextbtnunapply.TabIndex = 63;
            this.runasnonadmincontextbtnunapply.Text2 = "Undo";
            this.runasnonadmincontextbtnunapply.ButtonClick += new System.EventHandler(this.runasnonadmincontextbtnunapply_ButtonClick);
            // 
            // runasnonadmincontextbtnapply
            // 
            this.runasnonadmincontextbtnapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.runasnonadmincontextbtnapply.BackColor = System.Drawing.Color.Transparent;
            this.runasnonadmincontextbtnapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.runasnonadmincontextbtnapply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.runasnonadmincontextbtnapply.Enabled2 = true;
            this.runasnonadmincontextbtnapply.Location = new System.Drawing.Point(298, 149);
            this.runasnonadmincontextbtnapply.Name = "runasnonadmincontextbtnapply";
            this.runasnonadmincontextbtnapply.Size = new System.Drawing.Size(54, 17);
            this.runasnonadmincontextbtnapply.TabIndex = 62;
            this.runasnonadmincontextbtnapply.Text2 = "Apply";
            this.runasnonadmincontextbtnapply.ButtonClick += new System.EventHandler(this.runasnonadmincontextbtnapply_ButtonClick);
            // 
            // smallDots13
            // 
            this.smallDots13.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots13.BackColor = System.Drawing.Color.Transparent;
            this.smallDots13.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots13.BackgroundImage")));
            this.smallDots13.Location = new System.Drawing.Point(13, 132);
            this.smallDots13.Name = "smallDots13";
            this.smallDots13.Size = new System.Drawing.Size(397, 4);
            this.smallDots13.TabIndex = 61;
            // 
            // duckhunterundobtn
            // 
            this.duckhunterundobtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.duckhunterundobtn.BackColor = System.Drawing.Color.Transparent;
            this.duckhunterundobtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.duckhunterundobtn.Cursor = System.Windows.Forms.Cursors.No;
            this.duckhunterundobtn.Enabled2 = false;
            this.duckhunterundobtn.Location = new System.Drawing.Point(360, 109);
            this.duckhunterundobtn.Name = "duckhunterundobtn";
            this.duckhunterundobtn.Size = new System.Drawing.Size(51, 17);
            this.duckhunterundobtn.TabIndex = 59;
            this.duckhunterundobtn.Text2 = "Undo";
            this.duckhunterundobtn.ButtonClick += new System.EventHandler(this.duckhunterundobtn_ButtonClick);
            // 
            // duckhunterbtnapply
            // 
            this.duckhunterbtnapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.duckhunterbtnapply.BackColor = System.Drawing.Color.Transparent;
            this.duckhunterbtnapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.duckhunterbtnapply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.duckhunterbtnapply.Enabled2 = true;
            this.duckhunterbtnapply.Location = new System.Drawing.Point(300, 109);
            this.duckhunterbtnapply.Name = "duckhunterbtnapply";
            this.duckhunterbtnapply.Size = new System.Drawing.Size(54, 17);
            this.duckhunterbtnapply.TabIndex = 58;
            this.duckhunterbtnapply.Text2 = "Apply";
            this.duckhunterbtnapply.ButtonClick += new System.EventHandler(this.duckhunterbtnapply_ButtonClick);
            // 
            // smallDots12
            // 
            this.smallDots12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots12.BackColor = System.Drawing.Color.Transparent;
            this.smallDots12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots12.BackgroundImage")));
            this.smallDots12.Location = new System.Drawing.Point(15, 92);
            this.smallDots12.Name = "smallDots12";
            this.smallDots12.Size = new System.Drawing.Size(397, 4);
            this.smallDots12.TabIndex = 57;
            // 
            // spoolerbtnunapply
            // 
            this.spoolerbtnunapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spoolerbtnunapply.BackColor = System.Drawing.Color.Transparent;
            this.spoolerbtnunapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.spoolerbtnunapply.Cursor = System.Windows.Forms.Cursors.No;
            this.spoolerbtnunapply.Enabled2 = false;
            this.spoolerbtnunapply.Location = new System.Drawing.Point(358, 69);
            this.spoolerbtnunapply.Name = "spoolerbtnunapply";
            this.spoolerbtnunapply.Size = new System.Drawing.Size(51, 17);
            this.spoolerbtnunapply.TabIndex = 50;
            this.spoolerbtnunapply.Text2 = "Undo";
            this.spoolerbtnunapply.ButtonClick += new System.EventHandler(this.spoolerbtnunapply_ButtonClick);
            // 
            // spoolerbtnapply
            // 
            this.spoolerbtnapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.spoolerbtnapply.BackColor = System.Drawing.Color.Transparent;
            this.spoolerbtnapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.spoolerbtnapply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.spoolerbtnapply.Enabled2 = true;
            this.spoolerbtnapply.Location = new System.Drawing.Point(298, 69);
            this.spoolerbtnapply.Name = "spoolerbtnapply";
            this.spoolerbtnapply.Size = new System.Drawing.Size(54, 17);
            this.spoolerbtnapply.TabIndex = 49;
            this.spoolerbtnapply.Text2 = "Apply";
            this.spoolerbtnapply.ButtonClick += new System.EventHandler(this.spoolerbtnapply_ButtonClick);
            // 
            // smallDots11
            // 
            this.smallDots11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots11.BackColor = System.Drawing.Color.Transparent;
            this.smallDots11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots11.BackgroundImage")));
            this.smallDots11.Location = new System.Drawing.Point(13, 52);
            this.smallDots11.Name = "smallDots11";
            this.smallDots11.Size = new System.Drawing.Size(397, 4);
            this.smallDots11.TabIndex = 48;
            // 
            // mbrbtnunapply
            // 
            this.mbrbtnunapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mbrbtnunapply.BackColor = System.Drawing.Color.Transparent;
            this.mbrbtnunapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mbrbtnunapply.Cursor = System.Windows.Forms.Cursors.No;
            this.mbrbtnunapply.Enabled2 = false;
            this.mbrbtnunapply.Location = new System.Drawing.Point(358, 29);
            this.mbrbtnunapply.Name = "mbrbtnunapply";
            this.mbrbtnunapply.Size = new System.Drawing.Size(51, 17);
            this.mbrbtnunapply.TabIndex = 30;
            this.mbrbtnunapply.Text2 = "Undo";
            this.mbrbtnunapply.ButtonClick += new System.EventHandler(this.mbrbtnunapply_ButtonClick);
            // 
            // mbrapplybtn
            // 
            this.mbrapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.mbrapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.mbrapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.mbrapplybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mbrapplybtn.Enabled2 = true;
            this.mbrapplybtn.Location = new System.Drawing.Point(298, 29);
            this.mbrapplybtn.Name = "mbrapplybtn";
            this.mbrapplybtn.Size = new System.Drawing.Size(54, 17);
            this.mbrapplybtn.TabIndex = 29;
            this.mbrapplybtn.Text2 = "Apply";
            this.mbrapplybtn.ButtonClick += new System.EventHandler(this.mbrapplybtn_ButtonClick);
            // 
            // smallDots6
            // 
            this.smallDots6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots6.BackColor = System.Drawing.Color.Transparent;
            this.smallDots6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots6.BackgroundImage")));
            this.smallDots6.Location = new System.Drawing.Point(13, 12);
            this.smallDots6.Name = "smallDots6";
            this.smallDots6.Size = new System.Drawing.Size(397, 4);
            this.smallDots6.TabIndex = 28;
            // 
            // OptionalControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label13);
            this.Controls.Add(this.customButton1);
            this.Controls.Add(this.customButton2);
            this.Controls.Add(this.smallDots14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.runasnonadmincontextbtnunapply);
            this.Controls.Add(this.runasnonadmincontextbtnapply);
            this.Controls.Add(this.smallDots13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.duckhunterundobtn);
            this.Controls.Add(this.duckhunterbtnapply);
            this.Controls.Add(this.smallDots12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.spoolerbtnunapply);
            this.Controls.Add(this.spoolerbtnapply);
            this.Controls.Add(this.smallDots11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.mbrbtnunapply);
            this.Controls.Add(this.mbrapplybtn);
            this.Controls.Add(this.smallDots6);
            this.Name = "OptionalControl";
            this.Size = new System.Drawing.Size(424, 225);
            this.Load += new System.EventHandler(this.OptionalControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label6;
        private controls.CustomButton mbrbtnunapply;
        private controls.CustomButton mbrapplybtn;
        private controls.SmallDots smallDots6;
        private System.Windows.Forms.Label label11;
        private controls.CustomButton spoolerbtnunapply;
        private controls.CustomButton spoolerbtnapply;
        private controls.SmallDots smallDots11;
        private System.Windows.Forms.Label label14;
        private controls.CustomButton duckhunterundobtn;
        private controls.CustomButton duckhunterbtnapply;
        private controls.SmallDots smallDots12;
        private System.Windows.Forms.Label label15;
        private controls.CustomButton runasnonadmincontextbtnunapply;
        private controls.CustomButton runasnonadmincontextbtnapply;
        private controls.SmallDots smallDots13;
        private System.Windows.Forms.Label label13;
        private controls.CustomButton customButton1;
        private controls.CustomButton customButton2;
        private controls.SmallDots smallDots14;
    }
}
