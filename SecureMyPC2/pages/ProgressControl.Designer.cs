﻿
namespace SecureMyPC.pages
{
    partial class ProgressControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.progresslabel = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.progressstatus = new System.Windows.Forms.Label();
            this.undoallbtn = new SecureMyPC.controls.CustomButton();
            this.applyallbtn = new SecureMyPC.controls.CustomButton();
            this.roundedControl1 = new SecureMyPC.controls.SecureMyPC.controls.RoundedControl();
            this.progressindicator = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.progressindicator)).BeginInit();
            this.SuspendLayout();
            // 
            // progressBar1
            // 
            this.progressBar1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar1.Location = new System.Drawing.Point(18, 31);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(291, 17);
            this.progressBar1.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "Progress:";
            // 
            // progresslabel
            // 
            this.progresslabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.progresslabel.AutoSize = true;
            this.progresslabel.Location = new System.Drawing.Point(59, 16);
            this.progresslabel.Name = "progresslabel";
            this.progresslabel.Size = new System.Drawing.Size(18, 12);
            this.progresslabel.TabIndex = 4;
            this.progresslabel.Text = "0%";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(129, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 12);
            this.label2.TabIndex = 5;
            this.label2.Text = "Status:";
            // 
            // progressstatus
            // 
            this.progressstatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.progressstatus.AutoEllipsis = true;
            this.progressstatus.Location = new System.Drawing.Point(164, 16);
            this.progressstatus.Name = "progressstatus";
            this.progressstatus.Size = new System.Drawing.Size(126, 12);
            this.progressstatus.TabIndex = 6;
            this.progressstatus.Text = "...";
            // 
            // undoallbtn
            // 
            this.undoallbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.undoallbtn.BackColor = System.Drawing.Color.Transparent;
            this.undoallbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.undoallbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.undoallbtn.Enabled2 = true;
            this.undoallbtn.Location = new System.Drawing.Point(315, 8);
            this.undoallbtn.Name = "undoallbtn";
            this.undoallbtn.Size = new System.Drawing.Size(71, 17);
            this.undoallbtn.TabIndex = 1;
            this.undoallbtn.Text2 = "Undo All";
            this.undoallbtn.ButtonClick += new System.EventHandler(this.undoallbtn_ButtonClick);
            // 
            // applyallbtn
            // 
            this.applyallbtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.applyallbtn.BackColor = System.Drawing.Color.Transparent;
            this.applyallbtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.applyallbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.applyallbtn.Enabled2 = true;
            this.applyallbtn.Location = new System.Drawing.Point(315, 31);
            this.applyallbtn.Name = "applyallbtn";
            this.applyallbtn.Size = new System.Drawing.Size(71, 17);
            this.applyallbtn.TabIndex = 0;
            this.applyallbtn.Text2 = "Apply all";
            this.applyallbtn.ButtonClick += new System.EventHandler(this.applyallbtn_ButtonClick);
            // 
            // roundedControl1
            // 
            this.roundedControl1.Border = 0;
            this.roundedControl1.BorderColor = System.Drawing.Color.Transparent;
            this.roundedControl1.Radius = 30;
            this.roundedControl1.TargetControl = this;
            // 
            // progressindicator
            // 
            this.progressindicator.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.progressindicator.Image = global::SecureMyPC.Properties.Resources.loading_buffering;
            this.progressindicator.Location = new System.Drawing.Point(296, 15);
            this.progressindicator.Name = "progressindicator";
            this.progressindicator.Size = new System.Drawing.Size(13, 13);
            this.progressindicator.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.progressindicator.TabIndex = 7;
            this.progressindicator.TabStop = false;
            // 
            // ProgressControl
            // 
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.progressindicator);
            this.Controls.Add(this.progressstatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.progresslabel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.undoallbtn);
            this.Controls.Add(this.applyallbtn);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.DimGray;
            this.Name = "ProgressControl";
            this.Size = new System.Drawing.Size(399, 60);
            this.Load += new System.EventHandler(this.ProgressControl_Load_1);
            ((System.ComponentModel.ISupportInitialize)(this.progressindicator)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private controls.SecureMyPC.controls.RoundedControl roundedControl1;
        private controls.CustomButton undoallbtn;
        private controls.CustomButton applyallbtn;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label progresslabel;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label progressstatus;
        private System.Windows.Forms.PictureBox progressindicator;
    }
}
