﻿using SecureMyPC.controls;
using SecureMyPC.policies;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC.pages
{
    public partial class ProgressControl : UserControl
    {
        public ProgressControl()
        {
            InitializeComponent();
        }

        public Control TargetControl
        {
            get;set;
        }

        public bool IsProgressing
        {
            get;set;
        }

        public bool IgnoreSetProgress
        {
            get;set;
        }

        private void ProgressControl_Load_1(object sender, EventArgs e)
        {
            this.progressindicator.Visible = false;
        }

        public enum ProgressAction
        {
            APPLY,
            UNDO
        }

        public async void SetProgress(IPolicy policy, ProgressAction paction, CustomButton applybtn, CustomButton undobtn)
        {
            
            if (paction == ProgressAction.APPLY)
            {
                this.Invoke(new MethodInvoker(delegate
                {

                    this.progressindicator.Visible = true;
                    this.progressBar1.Value = 0;
                    this.progresslabel.Text = "0%";
                    this.progressstatus.Text = "applying " + policy.GetName();

                    applybtn.Enabled2 = false;
                    undobtn.Enabled2 = false;

                    applybtn.Update();
                    undobtn.Update();

                }));

                Task<bool> t = policy.Enable();
                await t;

                this.Invoke(new MethodInvoker(delegate
                {
                    this.progressindicator.Visible = false;
                    if (t.Result)
                    {
                        undobtn.Enabled2 = true;
                        applybtn.Enabled2 = false;
                        progressBar1.Value = 100;
                        progresslabel.Text = "100%";
                        this.progressstatus.Text = policy.GetName() + " done";
                        applybtn.Update();
                        undobtn.Update();
                    }
                    else
                    {
                        applybtn.Enabled2 = true;
                        undobtn.Enabled2 = false;
                        progressBar1.Value = 0;
                        progresslabel.Text = "0%";
                        this.progressstatus.Text = policy.GetName() + " failed!";
                        applybtn.Update();
                        undobtn.Update();
                    }
                }));
            }
            else if (paction == ProgressAction.UNDO)
            {
                this.Invoke(new MethodInvoker(delegate
                {

                    this.progressindicator.Visible = true;
                    this.progressBar1.Value = 0;
                    this.progresslabel.Text = "0%";
                    this.progressstatus.Text = "un-applying " + policy.GetName();

                    applybtn.Enabled2 = false;
                    undobtn.Enabled2 = false;

                    applybtn.Update();
                    undobtn.Update();

                }));

                Task<bool> t = policy.Disable();
                await t;

                this.Invoke(new MethodInvoker(delegate
                {
                    this.progressindicator.Visible = false;
                    if (t.Result)
                    {
                        applybtn.Enabled2 = true;
                        undobtn.Enabled2 = false;
                        progressBar1.Value = 100;
                        progresslabel.Text = "100%";
                        this.progressstatus.Text = policy.GetName() + " done";

                        applybtn.Update();
                        undobtn.Update();
                    }
                    else
                    {
                        undobtn.Enabled2 = true;
                        applybtn.Enabled2 = false;
                        progressBar1.Value = 0;
                        progresslabel.Text = "0%";
                        this.progressstatus.Text = policy.GetName() + " failed!";

                        applybtn.Update();
                        undobtn.Update();
                    }
                }));
            }
        

        }

        private async void applyallbtn_ButtonClick(object sender, EventArgs e)
        {
            IsProgressing = true;
            this.Invoke(new MethodInvoker(delegate
            {
                this.applyallbtn.Enabled2 = false;
                this.applyallbtn.Enabled = false;

                this.undoallbtn.Enabled2 = false;

                this.applyallbtn.Update();
                this.undoallbtn.Update();

            }));

            Task t = Task.Run(() =>
            {

                //first we check wether the control is a instane of main or optional.
                if (this.TargetControl == null)
                {
                    MessageBox.Show("Whoops this shouldn't happen!\nthere was no target control specified for the progress control!\nwe are unable to find our own buttons!", "Fatal error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (this.TargetControl is MainControl || this.TargetControl is OptionalControl)
                {
                    //we don't check between difference of this control since they are basicly identical, however.
                    //we now grab a list with all CustomButton controls which are named Apply.

                    ISet<CustomButton> buttons = new HashSet<CustomButton>();

                    foreach (Control c in this.TargetControl.Controls)
                    {
                        if (c is CustomButton)
                        {
                            CustomButton cs = (CustomButton)c;
                            if (cs.Text2 == "Apply" && cs.Enabled2)
                            {
                                buttons.Add(cs);
                            }
                        }
                    }

                    //when we are done we are going to apply the policies, we don't really need to use an order compared to wstt :)
                    foreach (CustomButton but in buttons.Reverse())
                    {
                        but.PerformClick();
                        Task t2 = Task.Delay(1000);
                        t2.Wait();
                    }

                }
            });
            await t;
            this.Invoke(new MethodInvoker(delegate
            {
                this.applyallbtn.Enabled2 = true;
                this.applyallbtn.Enabled = true;
                this.undoallbtn.Enabled2 = true;

                this.applyallbtn.Update();
                this.undoallbtn.Update();
            }));

            IsProgressing = false;
        }

        private async void undoallbtn_ButtonClick(object sender, EventArgs e)
        {
            this.IsProgressing = true;
            this.Invoke(new MethodInvoker(delegate
            {
                this.applyallbtn.Enabled2 = false;
                this.undoallbtn.Enabled2 = false;
                this.undoallbtn.Enabled = false;

                this.applyallbtn.Update();
                this.undoallbtn.Update();
            }));

            Task t = Task.Run(() => {

                //first we check wether the control is a instane of main or optional.
                if (this.TargetControl == null)
                {
                    MessageBox.Show("Whoops this shouldn't happen!\nthere was no target control specified for the progress control!\nwe are unable to find our own buttons!", "Fatal error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (this.TargetControl is MainControl || this.TargetControl is OptionalControl)
                {
                    //we don't check between difference of this control since they are basicly identical, however.
                    //we now grab a list with all CustomButton controls which are named Undo.

                    ISet<CustomButton> buttons = new HashSet<CustomButton>();

                    foreach (Control c in this.TargetControl.Controls)
                    {
                        if (c is CustomButton)
                        {
                            CustomButton cs = (CustomButton)c;
                            if (cs.Text2 == "Undo" && cs.Enabled2)
                            {
                                buttons.Add(cs);
                            }
                        }
                    }

                    //when we are done we are going to Undo the policies, we don't really need to use an order compared to wstt :)
                    foreach (CustomButton but in buttons.Reverse())
                    {
                        but.PerformClick();
                        Task t2 = Task.Delay(1000);
                        t2.Wait();
                    }
                }
             });
            await t;
            this.Invoke(new MethodInvoker(delegate
            {
                this.applyallbtn.Enabled2 = true;
                this.undoallbtn.Enabled2 = true;
                this.undoallbtn.Enabled = true;

                this.applyallbtn.Update();
                this.undoallbtn.Update();
            }));
            this.IsProgressing = false;
        }
    }
}
