﻿using SecureMyPC.pages;
using SecureMyPC.policies;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC
{
    public partial class MainControl : UserControl
    {
        public MainControl()
        {
            InitializeComponent();
        }

        private void MainControl_Load(object sender, EventArgs e)
        {
            this.DoStartupCheck();
        }

        public ProgressControl ProgressTarget { get; set; }

        public void DoStartupCheck()
        {
            foreach (PolicyType policy in PolicyType.Values())
            {
                //synchronize...
               // this.Invoke(new MethodInvoker(delegate
                //{
                    switch (policy.Name)
                    {

                        case "SRP_POLICY":
                            this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.srpapplybtn.Enabled2 = false;
                                    this.srpundobtn.Enabled2 = true;
                                }
                                else
                                {
                                    this.srpapplybtn.Enabled2 = true;
                                    this.srpundobtn.Enabled2 = false;
                                }
                            }));
                            break;
                        case "DEFAULT_SCRIPT_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.defaultscriptapplybtn.Enabled2 = false;
                                    this.defaultscriptundobtn.Enabled2 = true;
                                }
                                else
                                {
                                    this.defaultscriptapplybtn.Enabled2 = true;
                                    this.defaultscriptundobtn.Enabled2 = false;
                                }
                            }));
                            break;
                        case "WSCRIPT_SCRIPT_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.wscriptapplybtn.Enabled2 = false;
                                    this.wscriptunapplybtn.Enabled2 = true;
                                }
                                else
                                {
                                    this.wscriptapplybtn.Enabled2 = true;
                                    this.wscriptunapplybtn.Enabled2 = false;
                                }
                            }));
                            break;
                        case "UAC_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.uacapplybtn.Enabled2 = false;
                                    this.uacbtnunapply.Enabled2 = true;
                                }
                                else
                                {
                                    this.uacapplybtn.Enabled2 = true;
                                    this.uacbtnunapply.Enabled2 = false;
                                }
                            }));
                            break;
                        case "RDP_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.rdpbtnapply.Enabled2 = false;
                                    this.rdpbtnunapply.Enabled2 = true;
                                }
                                else
                                {
                                    this.rdpbtnapply.Enabled2 = true;
                                    this.rdpbtnunapply.Enabled2 = false;
                                }
                            }));
                            break;
                        case "REMOTE_REGISTRY_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.remoteregbtnapply.Enabled2 = false;
                                    this.remoteregbtnunapply.Enabled2 = true;
                                }
                                else
                                {
                                    this.remoteregbtnapply.Enabled2 = true;
                                    this.remoteregbtnunapply.Enabled2 = false;
                                }
                            }));
                            break;
                        case "ALWAYS_FORCE_ADMIN_PASSWORD_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.alwaysforceuacpasswordapplybtn.Enabled2 = false;
                                    this.alwaysforceuacpasswordunapplybtn.Enabled2 = true;
                                }
                                else
                                {
                                    this.alwaysforceuacpasswordapplybtn.Enabled2 = true;
                                    this.alwaysforceuacpasswordunapplybtn.Enabled2 = false;
                                }
                            }));
                            break;
                        case "AUTO_RUN_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.autorunapplybtn.Enabled2 = false;
                                    this.autorununapplybtn.Enabled2 = true;
                                }
                                else
                                {
                                    this.autorunapplybtn.Enabled2 = true;
                                    this.autorununapplybtn.Enabled2 = false;
                                }
                            }));
                            break;
                        case "NETBIOS_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.netbiosbtnapply.Enabled2 = false;
                                    this.netbiosbtnunapply.Enabled2 = true;
                                }
                                else
                                {
                                    this.netbiosbtnapply.Enabled2 = true;
                                    this.netbiosbtnunapply.Enabled2 = false;
                                }
                            }));
                            break;
                        case "RECENT_DOCUMENT_POLICY":
                        this.BeginInvoke(new MethodInvoker(delegate
                            {
                                if (policy.Policy.IsEnabled())
                                {
                                    this.recentdocbtnapply.Enabled2 = false;
                                    this.recentdocbtnunapply.Enabled2 = true;
                                }
                                else
                                {
                                    this.recentdocbtnapply.Enabled2 = true;
                                    this.recentdocbtnunapply.Enabled2 = false;
                                }
                            }));
                            break;
                    }
                //}));
            }
        }

        private void srpapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.SRP_POLICY.Policy, ProgressControl.ProgressAction.APPLY, srpapplybtn, srpundobtn);
        }

        private void srpundobtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.SRP_POLICY.Policy, ProgressControl.ProgressAction.UNDO, srpapplybtn, srpundobtn);
        }

        private void defaultscriptapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.DEFAULT_SCRIPT_POLICY.Policy, ProgressControl.ProgressAction.APPLY, defaultscriptapplybtn, defaultscriptundobtn);
        }

        private void defaultscriptundobtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.DEFAULT_SCRIPT_POLICY.Policy, ProgressControl.ProgressAction.UNDO, defaultscriptapplybtn, defaultscriptundobtn);
        }

        private void wscriptapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.WSCRIPT_SCRIPT_POLICY.Policy, ProgressControl.ProgressAction.APPLY, wscriptapplybtn, wscriptunapplybtn);
        }

        private void wscriptunapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.WSCRIPT_SCRIPT_POLICY.Policy, ProgressControl.ProgressAction.UNDO, wscriptapplybtn, wscriptunapplybtn);
        }

        private void uacapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.UAC_POLICY.Policy, ProgressControl.ProgressAction.APPLY, uacapplybtn, uacbtnunapply);
        }

        private void uacbtnunapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.UAC_POLICY.Policy, ProgressControl.ProgressAction.UNDO, uacapplybtn, uacbtnunapply);
        }

        private void rdpbtnapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.RDP_POLICY.Policy, ProgressControl.ProgressAction.APPLY, rdpbtnapply, rdpbtnunapply);
        }

        private void rdpbtnunapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.RDP_POLICY.Policy, ProgressControl.ProgressAction.UNDO, rdpbtnapply, rdpbtnunapply);
        }

        private void remoteregbtnapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.REMOTE_REGISTRY_POLICY.Policy, ProgressControl.ProgressAction.APPLY, remoteregbtnapply, remoteregbtnunapply);
        }

        private void remoteregbtnunapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.REMOTE_REGISTRY_POLICY.Policy, ProgressControl.ProgressAction.UNDO, remoteregbtnapply, remoteregbtnunapply);
        }

        private void alwaysforceuacpasswordapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.ALWAYS_FORCE_ADMIN_PASSWORD_POLICY.Policy, ProgressControl.ProgressAction.APPLY, alwaysforceuacpasswordapplybtn, alwaysforceuacpasswordunapplybtn);
        }

        private void alwaysforceuacpasswordunapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.ALWAYS_FORCE_ADMIN_PASSWORD_POLICY.Policy, ProgressControl.ProgressAction.UNDO, alwaysforceuacpasswordapplybtn, alwaysforceuacpasswordunapplybtn);
        }

        private void autorunapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.AUTORUN_POLICY.Policy, ProgressControl.ProgressAction.APPLY, autorunapplybtn, autorununapplybtn);
        }

        private void autorununapplybtn_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.AUTORUN_POLICY.Policy, ProgressControl.ProgressAction.UNDO, autorunapplybtn, autorununapplybtn);
        }

        private void netbiosbtnapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.NETBIOS_POLICY.Policy, ProgressControl.ProgressAction.APPLY, netbiosbtnapply, netbiosbtnunapply);
        }

        private void netbiosbtnunapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.NETBIOS_POLICY.Policy, ProgressControl.ProgressAction.UNDO, netbiosbtnapply, netbiosbtnunapply);
        }

        private void recentdocbtnapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.RECENT_DOCUMENT_POLICY.Policy, ProgressControl.ProgressAction.APPLY, recentdocbtnapply, recentdocbtnunapply);
        }

        private void recentdocbtnunapply_ButtonClick(object sender, EventArgs e)
        {
            this.ProgressTarget.SetProgress(PolicyType.RECENT_DOCUMENT_POLICY.Policy, ProgressControl.ProgressAction.UNDO, recentdocbtnapply, recentdocbtnunapply);
        }
    }
}
