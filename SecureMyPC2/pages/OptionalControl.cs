﻿using SecureMyPC.pages;
using SecureMyPC.policies;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC
{
    public partial class OptionalControl : UserControl
    {
        public OptionalControl()
        {
            InitializeComponent();
        }

        public ProgressControl ProgressTarget { get; set; }

        private void OptionalControl_Load(object sender, EventArgs e)
        {
            DoStartupCheck();
        }

        public void DoStartupCheck()
        {
            foreach (PolicyType p in PolicyType.Values())
            {
                if (p is OptionalPolicyType)
                {
                    switch(p.Name)
                    {
                        case "MBR_POLICY":
                            if(p.Policy.IsEnabled())
                            {
                                this.mbrapplybtn.Enabled2 = false;
                                this.mbrbtnunapply.Enabled2 = true;
                            } else
                            {
                                this.mbrapplybtn.Enabled2 = true;
                                this.mbrbtnunapply.Enabled2 = false;
                            }
                            break;
                        case "PRINT_SPOOLER_POLICY":
                            if (p.Policy.IsEnabled())
                            {
                                this.spoolerbtnapply.Enabled2 = false;
                                this.spoolerbtnunapply.Enabled2 = true;
                            }
                            else
                            {
                                this.spoolerbtnapply.Enabled2 = true;
                                this.spoolerbtnunapply.Enabled2 = false;
                            }
                            break;
                        case "DUCK_HUNTER_POLICY":
                            if (p.Policy.IsEnabled())
                            {
                                this.duckhunterbtnapply.Enabled2 = false;
                                this.duckhunterundobtn.Enabled2 = true;
                            }
                            else
                            {
                                this.duckhunterbtnapply.Enabled2 = true;
                                this.duckhunterundobtn.Enabled2 = false;
                            }
                            break;
                        case "RUN_AS_NON_ADMIN_CONTEXT_POLICY":
                            if (p.Policy.IsEnabled())
                            {
                                this.runasnonadmincontextbtnapply.Enabled2 = false;
                                this.runasnonadmincontextbtnunapply.Enabled2 = true;
                            }
                            else
                            {
                                this.runasnonadmincontextbtnapply.Enabled2 = true;
                                this.runasnonadmincontextbtnunapply.Enabled2 = false;
                            }
                            break;
                    }
                }
            }
        }

        private void mbrapplybtn_ButtonClick(object sender, EventArgs e)
        {
            ProgressTarget.SetProgress(PolicyType.MBR_POLICY.Policy, ProgressControl.ProgressAction.APPLY, this.mbrapplybtn, this.mbrbtnunapply);
        }

        private void mbrbtnunapply_ButtonClick(object sender, EventArgs e)
        {
            ProgressTarget.SetProgress(PolicyType.MBR_POLICY.Policy, ProgressControl.ProgressAction.UNDO, this.mbrapplybtn, this.mbrbtnunapply);
        }

        private void spoolerbtnapply_ButtonClick(object sender, EventArgs e)
        {
            ProgressTarget.SetProgress(PolicyType.PRINT_SPOOLER_POLICY.Policy, ProgressControl.ProgressAction.APPLY, this.spoolerbtnapply, this.spoolerbtnunapply);
        }

        private void spoolerbtnunapply_ButtonClick(object sender, EventArgs e)
        {
            ProgressTarget.SetProgress(PolicyType.PRINT_SPOOLER_POLICY.Policy, ProgressControl.ProgressAction.UNDO, this.spoolerbtnapply, this.spoolerbtnunapply);
        }

        private void duckhunterbtnapply_ButtonClick(object sender, EventArgs e)
        {
            ProgressTarget.SetProgress(PolicyType.DUCK_HUNTER_POLICY.Policy, ProgressControl.ProgressAction.APPLY, this.duckhunterbtnapply, this.duckhunterundobtn);
        }

        private void duckhunterundobtn_ButtonClick(object sender, EventArgs e)
        {
            ProgressTarget.SetProgress(PolicyType.DUCK_HUNTER_POLICY.Policy, ProgressControl.ProgressAction.UNDO, this.duckhunterbtnapply, this.duckhunterundobtn);
        }

        private void runasnonadmincontextbtnapply_ButtonClick(object sender, EventArgs e)
        {
            ProgressTarget.SetProgress(PolicyType.RUN_AS_NON_ADMIN_CONTEXT_POLICY.Policy, ProgressControl.ProgressAction.APPLY, this.runasnonadmincontextbtnapply, this.runasnonadmincontextbtnunapply);
        }

        private void runasnonadmincontextbtnunapply_ButtonClick(object sender, EventArgs e)
        {
            ProgressTarget.SetProgress(PolicyType.RUN_AS_NON_ADMIN_CONTEXT_POLICY.Policy, ProgressControl.ProgressAction.UNDO, this.runasnonadmincontextbtnapply, this.runasnonadmincontextbtnunapply);
        }
    }
}
