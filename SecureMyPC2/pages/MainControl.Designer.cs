﻿
namespace SecureMyPC
{
    partial class MainControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainControl));
            this.smallDots6 = new SecureMyPC.controls.SmallDots();
            this.label5 = new System.Windows.Forms.Label();
            this.rdpbtnunapply = new SecureMyPC.controls.CustomButton();
            this.rdpbtnapply = new SecureMyPC.controls.CustomButton();
            this.smallDots5 = new SecureMyPC.controls.SmallDots();
            this.label4 = new System.Windows.Forms.Label();
            this.uacbtnunapply = new SecureMyPC.controls.CustomButton();
            this.uacapplybtn = new SecureMyPC.controls.CustomButton();
            this.smallDots4 = new SecureMyPC.controls.SmallDots();
            this.label3 = new System.Windows.Forms.Label();
            this.wscriptunapplybtn = new SecureMyPC.controls.CustomButton();
            this.wscriptapplybtn = new SecureMyPC.controls.CustomButton();
            this.smallDots3 = new SecureMyPC.controls.SmallDots();
            this.label2 = new System.Windows.Forms.Label();
            this.defaultscriptundobtn = new SecureMyPC.controls.CustomButton();
            this.defaultscriptapplybtn = new SecureMyPC.controls.CustomButton();
            this.smallDots2 = new SecureMyPC.controls.SmallDots();
            this.label1 = new System.Windows.Forms.Label();
            this.srpundobtn = new SecureMyPC.controls.CustomButton();
            this.srpapplybtn = new SecureMyPC.controls.CustomButton();
            this.smallDots1 = new SecureMyPC.controls.SmallDots();
            this.label10 = new System.Windows.Forms.Label();
            this.netbiosbtnunapply = new SecureMyPC.controls.CustomButton();
            this.netbiosbtnapply = new SecureMyPC.controls.CustomButton();
            this.smallDots10 = new SecureMyPC.controls.SmallDots();
            this.label9 = new System.Windows.Forms.Label();
            this.autorununapplybtn = new SecureMyPC.controls.CustomButton();
            this.autorunapplybtn = new SecureMyPC.controls.CustomButton();
            this.smallDots9 = new SecureMyPC.controls.SmallDots();
            this.label8 = new System.Windows.Forms.Label();
            this.alwaysforceuacpasswordunapplybtn = new SecureMyPC.controls.CustomButton();
            this.alwaysforceuacpasswordapplybtn = new SecureMyPC.controls.CustomButton();
            this.smallDots8 = new SecureMyPC.controls.SmallDots();
            this.label7 = new System.Windows.Forms.Label();
            this.remoteregbtnunapply = new SecureMyPC.controls.CustomButton();
            this.remoteregbtnapply = new SecureMyPC.controls.CustomButton();
            this.label6 = new System.Windows.Forms.Label();
            this.recentdocbtnunapply = new SecureMyPC.controls.CustomButton();
            this.recentdocbtnapply = new SecureMyPC.controls.CustomButton();
            this.smallDots7 = new SecureMyPC.controls.SmallDots();
            this.label11 = new System.Windows.Forms.Label();
            this.customButton1 = new SecureMyPC.controls.CustomButton();
            this.customButton2 = new SecureMyPC.controls.CustomButton();
            this.smallDots11 = new SecureMyPC.controls.SmallDots();
            this.SuspendLayout();
            // 
            // smallDots6
            // 
            this.smallDots6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots6.BackColor = System.Drawing.Color.Transparent;
            this.smallDots6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots6.BackgroundImage")));
            this.smallDots6.Location = new System.Drawing.Point(15, 222);
            this.smallDots6.Name = "smallDots6";
            this.smallDots6.Size = new System.Drawing.Size(397, 4);
            this.smallDots6.TabIndex = 45;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DimGray;
            this.label5.Location = new System.Drawing.Point(15, 196);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 12);
            this.label5.TabIndex = 44;
            this.label5.Text = "Disable remote assistance";
            // 
            // rdpbtnunapply
            // 
            this.rdpbtnunapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdpbtnunapply.BackColor = System.Drawing.Color.Transparent;
            this.rdpbtnunapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.rdpbtnunapply.Cursor = System.Windows.Forms.Cursors.No;
            this.rdpbtnunapply.Enabled2 = false;
            this.rdpbtnunapply.Location = new System.Drawing.Point(360, 196);
            this.rdpbtnunapply.Name = "rdpbtnunapply";
            this.rdpbtnunapply.Size = new System.Drawing.Size(51, 17);
            this.rdpbtnunapply.TabIndex = 43;
            this.rdpbtnunapply.Text2 = "Undo";
            this.rdpbtnunapply.ButtonClick += new System.EventHandler(this.rdpbtnunapply_ButtonClick);
            // 
            // rdpbtnapply
            // 
            this.rdpbtnapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.rdpbtnapply.BackColor = System.Drawing.Color.Transparent;
            this.rdpbtnapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.rdpbtnapply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rdpbtnapply.Enabled2 = true;
            this.rdpbtnapply.Location = new System.Drawing.Point(300, 196);
            this.rdpbtnapply.Name = "rdpbtnapply";
            this.rdpbtnapply.Size = new System.Drawing.Size(54, 17);
            this.rdpbtnapply.TabIndex = 42;
            this.rdpbtnapply.Text2 = "Apply";
            this.rdpbtnapply.ButtonClick += new System.EventHandler(this.rdpbtnapply_ButtonClick);
            // 
            // smallDots5
            // 
            this.smallDots5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots5.BackColor = System.Drawing.Color.Transparent;
            this.smallDots5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots5.BackgroundImage")));
            this.smallDots5.Location = new System.Drawing.Point(15, 178);
            this.smallDots5.Name = "smallDots5";
            this.smallDots5.Size = new System.Drawing.Size(397, 4);
            this.smallDots5.TabIndex = 41;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DimGray;
            this.label4.Location = new System.Drawing.Point(15, 152);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(88, 12);
            this.label4.TabIndex = 40;
            this.label4.Text = "Set UAC on high";
            // 
            // uacbtnunapply
            // 
            this.uacbtnunapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uacbtnunapply.BackColor = System.Drawing.Color.Transparent;
            this.uacbtnunapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.uacbtnunapply.Cursor = System.Windows.Forms.Cursors.No;
            this.uacbtnunapply.Enabled2 = false;
            this.uacbtnunapply.Location = new System.Drawing.Point(360, 152);
            this.uacbtnunapply.Name = "uacbtnunapply";
            this.uacbtnunapply.Size = new System.Drawing.Size(51, 17);
            this.uacbtnunapply.TabIndex = 39;
            this.uacbtnunapply.Text2 = "Undo";
            this.uacbtnunapply.ButtonClick += new System.EventHandler(this.uacbtnunapply_ButtonClick);
            // 
            // uacapplybtn
            // 
            this.uacapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.uacapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.uacapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.uacapplybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.uacapplybtn.Enabled2 = true;
            this.uacapplybtn.Location = new System.Drawing.Point(300, 152);
            this.uacapplybtn.Name = "uacapplybtn";
            this.uacapplybtn.Size = new System.Drawing.Size(54, 17);
            this.uacapplybtn.TabIndex = 38;
            this.uacapplybtn.Text2 = "Apply";
            this.uacapplybtn.ButtonClick += new System.EventHandler(this.uacapplybtn_ButtonClick);
            // 
            // smallDots4
            // 
            this.smallDots4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots4.BackColor = System.Drawing.Color.Transparent;
            this.smallDots4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots4.BackgroundImage")));
            this.smallDots4.Location = new System.Drawing.Point(15, 135);
            this.smallDots4.Name = "smallDots4";
            this.smallDots4.Size = new System.Drawing.Size(397, 4);
            this.smallDots4.TabIndex = 37;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DimGray;
            this.label3.Location = new System.Drawing.Point(15, 109);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 12);
            this.label3.TabIndex = 36;
            this.label3.Text = "Disable Wscript";
            // 
            // wscriptunapplybtn
            // 
            this.wscriptunapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.wscriptunapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.wscriptunapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.wscriptunapplybtn.Cursor = System.Windows.Forms.Cursors.No;
            this.wscriptunapplybtn.Enabled2 = false;
            this.wscriptunapplybtn.Location = new System.Drawing.Point(360, 109);
            this.wscriptunapplybtn.Name = "wscriptunapplybtn";
            this.wscriptunapplybtn.Size = new System.Drawing.Size(51, 17);
            this.wscriptunapplybtn.TabIndex = 35;
            this.wscriptunapplybtn.Text2 = "Undo";
            this.wscriptunapplybtn.ButtonClick += new System.EventHandler(this.wscriptunapplybtn_ButtonClick);
            // 
            // wscriptapplybtn
            // 
            this.wscriptapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.wscriptapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.wscriptapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.wscriptapplybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.wscriptapplybtn.Enabled2 = true;
            this.wscriptapplybtn.Location = new System.Drawing.Point(300, 109);
            this.wscriptapplybtn.Name = "wscriptapplybtn";
            this.wscriptapplybtn.Size = new System.Drawing.Size(54, 17);
            this.wscriptapplybtn.TabIndex = 34;
            this.wscriptapplybtn.Text2 = "Apply";
            this.wscriptapplybtn.ButtonClick += new System.EventHandler(this.wscriptapplybtn_ButtonClick);
            // 
            // smallDots3
            // 
            this.smallDots3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots3.BackColor = System.Drawing.Color.Transparent;
            this.smallDots3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots3.BackgroundImage")));
            this.smallDots3.Location = new System.Drawing.Point(15, 93);
            this.smallDots3.Name = "smallDots3";
            this.smallDots3.Size = new System.Drawing.Size(397, 4);
            this.smallDots3.TabIndex = 33;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DimGray;
            this.label2.Location = new System.Drawing.Point(15, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(228, 12);
            this.label2.TabIndex = 32;
            this.label2.Text = "Set default program for all scripts to notepad";
            // 
            // defaultscriptundobtn
            // 
            this.defaultscriptundobtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultscriptundobtn.BackColor = System.Drawing.Color.Transparent;
            this.defaultscriptundobtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.defaultscriptundobtn.Cursor = System.Windows.Forms.Cursors.No;
            this.defaultscriptundobtn.Enabled2 = false;
            this.defaultscriptundobtn.Location = new System.Drawing.Point(360, 67);
            this.defaultscriptundobtn.Name = "defaultscriptundobtn";
            this.defaultscriptundobtn.Size = new System.Drawing.Size(51, 17);
            this.defaultscriptundobtn.TabIndex = 31;
            this.defaultscriptundobtn.Text2 = "Undo";
            this.defaultscriptundobtn.ButtonClick += new System.EventHandler(this.defaultscriptundobtn_ButtonClick);
            // 
            // defaultscriptapplybtn
            // 
            this.defaultscriptapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.defaultscriptapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.defaultscriptapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.defaultscriptapplybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.defaultscriptapplybtn.Enabled2 = true;
            this.defaultscriptapplybtn.Location = new System.Drawing.Point(300, 67);
            this.defaultscriptapplybtn.Name = "defaultscriptapplybtn";
            this.defaultscriptapplybtn.Size = new System.Drawing.Size(54, 17);
            this.defaultscriptapplybtn.TabIndex = 30;
            this.defaultscriptapplybtn.Text2 = "Apply";
            this.defaultscriptapplybtn.ButtonClick += new System.EventHandler(this.defaultscriptapplybtn_ButtonClick);
            // 
            // smallDots2
            // 
            this.smallDots2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots2.BackColor = System.Drawing.Color.Transparent;
            this.smallDots2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots2.BackgroundImage")));
            this.smallDots2.Location = new System.Drawing.Point(15, 51);
            this.smallDots2.Name = "smallDots2";
            this.smallDots2.Size = new System.Drawing.Size(397, 4);
            this.smallDots2.TabIndex = 29;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(15, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 12);
            this.label1.TabIndex = 28;
            this.label1.Text = "Set software restriction policies";
            // 
            // srpundobtn
            // 
            this.srpundobtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.srpundobtn.BackColor = System.Drawing.Color.Transparent;
            this.srpundobtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.srpundobtn.Cursor = System.Windows.Forms.Cursors.No;
            this.srpundobtn.Enabled2 = false;
            this.srpundobtn.Location = new System.Drawing.Point(360, 25);
            this.srpundobtn.Name = "srpundobtn";
            this.srpundobtn.Size = new System.Drawing.Size(51, 17);
            this.srpundobtn.TabIndex = 27;
            this.srpundobtn.Text2 = "Undo";
            this.srpundobtn.ButtonClick += new System.EventHandler(this.srpundobtn_ButtonClick);
            // 
            // srpapplybtn
            // 
            this.srpapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.srpapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.srpapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.srpapplybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.srpapplybtn.Enabled2 = true;
            this.srpapplybtn.Location = new System.Drawing.Point(300, 25);
            this.srpapplybtn.Name = "srpapplybtn";
            this.srpapplybtn.Size = new System.Drawing.Size(54, 17);
            this.srpapplybtn.TabIndex = 26;
            this.srpapplybtn.Text2 = "Apply";
            this.srpapplybtn.ButtonClick += new System.EventHandler(this.srpapplybtn_ButtonClick);
            // 
            // smallDots1
            // 
            this.smallDots1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots1.BackColor = System.Drawing.Color.Transparent;
            this.smallDots1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots1.BackgroundImage")));
            this.smallDots1.Location = new System.Drawing.Point(15, 13);
            this.smallDots1.Name = "smallDots1";
            this.smallDots1.Size = new System.Drawing.Size(397, 4);
            this.smallDots1.TabIndex = 25;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.DimGray;
            this.label10.Location = new System.Drawing.Point(13, 366);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(83, 12);
            this.label10.TabIndex = 60;
            this.label10.Text = "Disable netbios";
            // 
            // netbiosbtnunapply
            // 
            this.netbiosbtnunapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.netbiosbtnunapply.BackColor = System.Drawing.Color.Transparent;
            this.netbiosbtnunapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.netbiosbtnunapply.Cursor = System.Windows.Forms.Cursors.No;
            this.netbiosbtnunapply.Enabled2 = false;
            this.netbiosbtnunapply.Location = new System.Drawing.Point(358, 366);
            this.netbiosbtnunapply.Name = "netbiosbtnunapply";
            this.netbiosbtnunapply.Size = new System.Drawing.Size(51, 17);
            this.netbiosbtnunapply.TabIndex = 59;
            this.netbiosbtnunapply.Text2 = "Undo";
            this.netbiosbtnunapply.ButtonClick += new System.EventHandler(this.netbiosbtnunapply_ButtonClick);
            // 
            // netbiosbtnapply
            // 
            this.netbiosbtnapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.netbiosbtnapply.BackColor = System.Drawing.Color.Transparent;
            this.netbiosbtnapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.netbiosbtnapply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.netbiosbtnapply.Enabled2 = true;
            this.netbiosbtnapply.Location = new System.Drawing.Point(298, 366);
            this.netbiosbtnapply.Name = "netbiosbtnapply";
            this.netbiosbtnapply.Size = new System.Drawing.Size(54, 17);
            this.netbiosbtnapply.TabIndex = 58;
            this.netbiosbtnapply.Text2 = "Apply";
            this.netbiosbtnapply.ButtonClick += new System.EventHandler(this.netbiosbtnapply_ButtonClick);
            // 
            // smallDots10
            // 
            this.smallDots10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots10.BackColor = System.Drawing.Color.Transparent;
            this.smallDots10.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots10.BackgroundImage")));
            this.smallDots10.Location = new System.Drawing.Point(15, 351);
            this.smallDots10.Name = "smallDots10";
            this.smallDots10.Size = new System.Drawing.Size(397, 4);
            this.smallDots10.TabIndex = 57;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.DimGray;
            this.label9.Location = new System.Drawing.Point(13, 325);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 12);
            this.label9.TabIndex = 56;
            this.label9.Text = "Disable Autoplay";
            // 
            // autorununapplybtn
            // 
            this.autorununapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.autorununapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.autorununapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.autorununapplybtn.Cursor = System.Windows.Forms.Cursors.No;
            this.autorununapplybtn.Enabled2 = false;
            this.autorununapplybtn.Location = new System.Drawing.Point(358, 325);
            this.autorununapplybtn.Name = "autorununapplybtn";
            this.autorununapplybtn.Size = new System.Drawing.Size(51, 17);
            this.autorununapplybtn.TabIndex = 55;
            this.autorununapplybtn.Text2 = "Undo";
            this.autorununapplybtn.ButtonClick += new System.EventHandler(this.autorununapplybtn_ButtonClick);
            // 
            // autorunapplybtn
            // 
            this.autorunapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.autorunapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.autorunapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.autorunapplybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.autorunapplybtn.Enabled2 = true;
            this.autorunapplybtn.Location = new System.Drawing.Point(298, 325);
            this.autorunapplybtn.Name = "autorunapplybtn";
            this.autorunapplybtn.Size = new System.Drawing.Size(54, 17);
            this.autorunapplybtn.TabIndex = 54;
            this.autorunapplybtn.Text2 = "Apply";
            this.autorunapplybtn.ButtonClick += new System.EventHandler(this.autorunapplybtn_ButtonClick);
            // 
            // smallDots9
            // 
            this.smallDots9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots9.BackColor = System.Drawing.Color.Transparent;
            this.smallDots9.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots9.BackgroundImage")));
            this.smallDots9.Location = new System.Drawing.Point(13, 311);
            this.smallDots9.Name = "smallDots9";
            this.smallDots9.Size = new System.Drawing.Size(397, 4);
            this.smallDots9.TabIndex = 53;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(13, 285);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(170, 12);
            this.label8.TabIndex = 52;
            this.label8.Text = "Always Force password on admin";
            // 
            // alwaysforceuacpasswordunapplybtn
            // 
            this.alwaysforceuacpasswordunapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.alwaysforceuacpasswordunapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.alwaysforceuacpasswordunapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.alwaysforceuacpasswordunapplybtn.Cursor = System.Windows.Forms.Cursors.No;
            this.alwaysforceuacpasswordunapplybtn.Enabled2 = false;
            this.alwaysforceuacpasswordunapplybtn.Location = new System.Drawing.Point(358, 285);
            this.alwaysforceuacpasswordunapplybtn.Name = "alwaysforceuacpasswordunapplybtn";
            this.alwaysforceuacpasswordunapplybtn.Size = new System.Drawing.Size(51, 17);
            this.alwaysforceuacpasswordunapplybtn.TabIndex = 51;
            this.alwaysforceuacpasswordunapplybtn.Text2 = "Undo";
            this.alwaysforceuacpasswordunapplybtn.ButtonClick += new System.EventHandler(this.alwaysforceuacpasswordunapplybtn_ButtonClick);
            // 
            // alwaysforceuacpasswordapplybtn
            // 
            this.alwaysforceuacpasswordapplybtn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.alwaysforceuacpasswordapplybtn.BackColor = System.Drawing.Color.Transparent;
            this.alwaysforceuacpasswordapplybtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.alwaysforceuacpasswordapplybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.alwaysforceuacpasswordapplybtn.Enabled2 = true;
            this.alwaysforceuacpasswordapplybtn.Location = new System.Drawing.Point(298, 285);
            this.alwaysforceuacpasswordapplybtn.Name = "alwaysforceuacpasswordapplybtn";
            this.alwaysforceuacpasswordapplybtn.Size = new System.Drawing.Size(54, 17);
            this.alwaysforceuacpasswordapplybtn.TabIndex = 50;
            this.alwaysforceuacpasswordapplybtn.Text2 = "Apply";
            this.alwaysforceuacpasswordapplybtn.ButtonClick += new System.EventHandler(this.alwaysforceuacpasswordapplybtn_ButtonClick);
            // 
            // smallDots8
            // 
            this.smallDots8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots8.BackColor = System.Drawing.Color.Transparent;
            this.smallDots8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots8.BackgroundImage")));
            this.smallDots8.Location = new System.Drawing.Point(13, 266);
            this.smallDots8.Name = "smallDots8";
            this.smallDots8.Size = new System.Drawing.Size(397, 4);
            this.smallDots8.TabIndex = 49;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DimGray;
            this.label7.Location = new System.Drawing.Point(13, 240);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(129, 12);
            this.label7.TabIndex = 48;
            this.label7.Text = "Disable Remote Registry";
            // 
            // remoteregbtnunapply
            // 
            this.remoteregbtnunapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.remoteregbtnunapply.BackColor = System.Drawing.Color.Transparent;
            this.remoteregbtnunapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.remoteregbtnunapply.Cursor = System.Windows.Forms.Cursors.No;
            this.remoteregbtnunapply.Enabled2 = false;
            this.remoteregbtnunapply.Location = new System.Drawing.Point(358, 240);
            this.remoteregbtnunapply.Name = "remoteregbtnunapply";
            this.remoteregbtnunapply.Size = new System.Drawing.Size(51, 17);
            this.remoteregbtnunapply.TabIndex = 47;
            this.remoteregbtnunapply.Text2 = "Undo";
            this.remoteregbtnunapply.ButtonClick += new System.EventHandler(this.remoteregbtnunapply_ButtonClick);
            // 
            // remoteregbtnapply
            // 
            this.remoteregbtnapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.remoteregbtnapply.BackColor = System.Drawing.Color.Transparent;
            this.remoteregbtnapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.remoteregbtnapply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.remoteregbtnapply.Enabled2 = true;
            this.remoteregbtnapply.Location = new System.Drawing.Point(298, 240);
            this.remoteregbtnapply.Name = "remoteregbtnapply";
            this.remoteregbtnapply.Size = new System.Drawing.Size(54, 17);
            this.remoteregbtnapply.TabIndex = 46;
            this.remoteregbtnapply.Text2 = "Apply";
            this.remoteregbtnapply.ButtonClick += new System.EventHandler(this.remoteregbtnapply_ButtonClick);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DimGray;
            this.label6.Location = new System.Drawing.Point(11, 406);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(168, 12);
            this.label6.TabIndex = 64;
            this.label6.Text = "Disable recent document history";
            // 
            // recentdocbtnunapply
            // 
            this.recentdocbtnunapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.recentdocbtnunapply.BackColor = System.Drawing.Color.Transparent;
            this.recentdocbtnunapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.recentdocbtnunapply.Cursor = System.Windows.Forms.Cursors.No;
            this.recentdocbtnunapply.Enabled2 = false;
            this.recentdocbtnunapply.Location = new System.Drawing.Point(356, 406);
            this.recentdocbtnunapply.Name = "recentdocbtnunapply";
            this.recentdocbtnunapply.Size = new System.Drawing.Size(51, 17);
            this.recentdocbtnunapply.TabIndex = 63;
            this.recentdocbtnunapply.Text2 = "Undo";
            this.recentdocbtnunapply.ButtonClick += new System.EventHandler(this.recentdocbtnunapply_ButtonClick);
            // 
            // recentdocbtnapply
            // 
            this.recentdocbtnapply.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.recentdocbtnapply.BackColor = System.Drawing.Color.Transparent;
            this.recentdocbtnapply.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.recentdocbtnapply.Cursor = System.Windows.Forms.Cursors.Hand;
            this.recentdocbtnapply.Enabled2 = true;
            this.recentdocbtnapply.Location = new System.Drawing.Point(296, 406);
            this.recentdocbtnapply.Name = "recentdocbtnapply";
            this.recentdocbtnapply.Size = new System.Drawing.Size(54, 17);
            this.recentdocbtnapply.TabIndex = 62;
            this.recentdocbtnapply.Text2 = "Apply";
            this.recentdocbtnapply.ButtonClick += new System.EventHandler(this.recentdocbtnapply_ButtonClick);
            // 
            // smallDots7
            // 
            this.smallDots7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots7.BackColor = System.Drawing.Color.Transparent;
            this.smallDots7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots7.BackgroundImage")));
            this.smallDots7.Location = new System.Drawing.Point(13, 391);
            this.smallDots7.Name = "smallDots7";
            this.smallDots7.Size = new System.Drawing.Size(397, 4);
            this.smallDots7.TabIndex = 61;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Verdana", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.DimGray;
            this.label11.Location = new System.Drawing.Point(11, 446);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(269, 12);
            this.label11.TabIndex = 68;
            this.label11.Text = "Disable powershell via policy [to block duckyscripts]";
            // 
            // customButton1
            // 
            this.customButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.customButton1.BackColor = System.Drawing.Color.Transparent;
            this.customButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customButton1.Cursor = System.Windows.Forms.Cursors.No;
            this.customButton1.Enabled2 = false;
            this.customButton1.Location = new System.Drawing.Point(356, 446);
            this.customButton1.Name = "customButton1";
            this.customButton1.Size = new System.Drawing.Size(51, 17);
            this.customButton1.TabIndex = 67;
            this.customButton1.Text2 = "Undo";
            // 
            // customButton2
            // 
            this.customButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.customButton2.BackColor = System.Drawing.Color.Transparent;
            this.customButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customButton2.Enabled2 = true;
            this.customButton2.Location = new System.Drawing.Point(296, 446);
            this.customButton2.Name = "customButton2";
            this.customButton2.Size = new System.Drawing.Size(54, 17);
            this.customButton2.TabIndex = 66;
            this.customButton2.Text2 = "Apply";
            // 
            // smallDots11
            // 
            this.smallDots11.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.smallDots11.BackColor = System.Drawing.Color.Transparent;
            this.smallDots11.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("smallDots11.BackgroundImage")));
            this.smallDots11.Location = new System.Drawing.Point(13, 431);
            this.smallDots11.Name = "smallDots11";
            this.smallDots11.Size = new System.Drawing.Size(397, 4);
            this.smallDots11.TabIndex = 65;
            // 
            // MainControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label11);
            this.Controls.Add(this.customButton1);
            this.Controls.Add(this.customButton2);
            this.Controls.Add(this.smallDots11);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.recentdocbtnunapply);
            this.Controls.Add(this.recentdocbtnapply);
            this.Controls.Add(this.smallDots7);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.netbiosbtnunapply);
            this.Controls.Add(this.netbiosbtnapply);
            this.Controls.Add(this.smallDots10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.autorununapplybtn);
            this.Controls.Add(this.autorunapplybtn);
            this.Controls.Add(this.smallDots9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.alwaysforceuacpasswordunapplybtn);
            this.Controls.Add(this.alwaysforceuacpasswordapplybtn);
            this.Controls.Add(this.smallDots8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.remoteregbtnunapply);
            this.Controls.Add(this.remoteregbtnapply);
            this.Controls.Add(this.smallDots6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.rdpbtnunapply);
            this.Controls.Add(this.rdpbtnapply);
            this.Controls.Add(this.smallDots5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.uacbtnunapply);
            this.Controls.Add(this.uacapplybtn);
            this.Controls.Add(this.smallDots4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.wscriptunapplybtn);
            this.Controls.Add(this.wscriptapplybtn);
            this.Controls.Add(this.smallDots3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.defaultscriptundobtn);
            this.Controls.Add(this.defaultscriptapplybtn);
            this.Controls.Add(this.smallDots2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.srpundobtn);
            this.Controls.Add(this.srpapplybtn);
            this.Controls.Add(this.smallDots1);
            this.Name = "MainControl";
            this.Size = new System.Drawing.Size(424, 485);
            this.Load += new System.EventHandler(this.MainControl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private controls.SmallDots smallDots6;
        private System.Windows.Forms.Label label5;
        private controls.SmallDots smallDots5;
        private System.Windows.Forms.Label label4;
        private controls.SmallDots smallDots4;
        private System.Windows.Forms.Label label3;
        private controls.SmallDots smallDots3;
        private System.Windows.Forms.Label label2;
        private controls.SmallDots smallDots2;
        private System.Windows.Forms.Label label1;
        private controls.SmallDots smallDots1;
        private System.Windows.Forms.Label label10;
        private controls.SmallDots smallDots10;
        private System.Windows.Forms.Label label9;
        private controls.SmallDots smallDots9;
        private System.Windows.Forms.Label label8;
        private controls.SmallDots smallDots8;
        private System.Windows.Forms.Label label7;
        public controls.CustomButton rdpbtnunapply;
        public controls.CustomButton rdpbtnapply;
        public controls.CustomButton uacbtnunapply;
        public controls.CustomButton uacapplybtn;
        public controls.CustomButton wscriptunapplybtn;
        public controls.CustomButton wscriptapplybtn;
        public controls.CustomButton defaultscriptundobtn;
        public controls.CustomButton defaultscriptapplybtn;
        public controls.CustomButton srpundobtn;
        public controls.CustomButton srpapplybtn;
        public controls.CustomButton netbiosbtnunapply;
        public controls.CustomButton netbiosbtnapply;
        public controls.CustomButton autorununapplybtn;
        public controls.CustomButton autorunapplybtn;
        public controls.CustomButton alwaysforceuacpasswordunapplybtn;
        public controls.CustomButton alwaysforceuacpasswordapplybtn;
        public controls.CustomButton remoteregbtnunapply;
        public controls.CustomButton remoteregbtnapply;
        private System.Windows.Forms.Label label6;
        public controls.CustomButton recentdocbtnunapply;
        public controls.CustomButton recentdocbtnapply;
        private controls.SmallDots smallDots7;
        private System.Windows.Forms.Label label11;
        public controls.CustomButton customButton1;
        public controls.CustomButton customButton2;
        private controls.SmallDots smallDots11;
    }
}
