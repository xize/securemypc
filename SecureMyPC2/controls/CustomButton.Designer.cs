﻿
namespace SecureMyPC.controls
{
    partial class CustomButton
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.center = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.rightcorner = new System.Windows.Forms.Panel();
            this.leftcorner = new System.Windows.Forms.Panel();
            this.center.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.panel2.Location = new System.Drawing.Point(8, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(0, 10);
            this.panel2.TabIndex = 1;
            this.panel2.Click += new System.EventHandler(this.label1_Click);
            // 
            // center
            // 
            this.center.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.center.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.center.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.center.Controls.Add(this.label1);
            this.center.Location = new System.Drawing.Point(7, 0);
            this.center.Name = "center";
            this.center.Size = new System.Drawing.Size(54, 17);
            this.center.TabIndex = 1;
            this.center.Click += new System.EventHandler(this.label1_Click);
            this.center.MouseEnter += new System.EventHandler(this.CustomButton_MouseEnter);
            this.center.MouseLeave += new System.EventHandler(this.CustomButton_MouseLeave);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Font = new System.Drawing.Font("Impact", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "OK";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            this.label1.MouseEnter += new System.EventHandler(this.CustomButton_MouseEnter);
            this.label1.MouseLeave += new System.EventHandler(this.CustomButton_MouseLeave);
            // 
            // rightcorner
            // 
            this.rightcorner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rightcorner.BackColor = System.Drawing.Color.Transparent;
            this.rightcorner.BackgroundImage = global::SecureMyPC.Properties.Resources.btnright;
            this.rightcorner.Location = new System.Drawing.Point(61, 0);
            this.rightcorner.Name = "rightcorner";
            this.rightcorner.Size = new System.Drawing.Size(8, 17);
            this.rightcorner.TabIndex = 1;
            this.rightcorner.Click += new System.EventHandler(this.label1_Click);
            this.rightcorner.MouseEnter += new System.EventHandler(this.CustomButton_MouseEnter);
            this.rightcorner.MouseLeave += new System.EventHandler(this.CustomButton_MouseLeave);
            // 
            // leftcorner
            // 
            this.leftcorner.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.leftcorner.BackColor = System.Drawing.Color.Transparent;
            this.leftcorner.BackgroundImage = global::SecureMyPC.Properties.Resources.btn_left;
            this.leftcorner.Location = new System.Drawing.Point(0, 0);
            this.leftcorner.Name = "leftcorner";
            this.leftcorner.Size = new System.Drawing.Size(8, 17);
            this.leftcorner.TabIndex = 0;
            this.leftcorner.Click += new System.EventHandler(this.label1_Click);
            this.leftcorner.MouseEnter += new System.EventHandler(this.CustomButton_MouseEnter);
            this.leftcorner.MouseLeave += new System.EventHandler(this.CustomButton_MouseLeave);
            // 
            // CustomButton
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Controls.Add(this.center);
            this.Controls.Add(this.rightcorner);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.leftcorner);
            this.Name = "CustomButton";
            this.Size = new System.Drawing.Size(71, 17);
            this.MouseEnter += new System.EventHandler(this.CustomButton_MouseEnter);
            this.MouseLeave += new System.EventHandler(this.CustomButton_MouseLeave);
            this.center.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel leftcorner;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel center;
        private System.Windows.Forms.Panel rightcorner;
        private System.Windows.Forms.Label label1;
    }
}
