﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC.controls
{
    public partial class CustomButton : UserControl
    {
        private bool enabled = true;

        public event EventHandler ButtonClick;

        public CustomButton()
        {
            InitializeComponent();
        }

        public async void PerformClick()
        {
            EventHandler handler = ButtonClick;
            if(handler != null)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.Enabled2 = false;
                    this.Cursor = Cursors.WaitCursor;
                }));

                await Task.Run(() => handler?.Invoke(this, EventArgs.Empty));

                this.Invoke(new MethodInvoker(delegate
                {
                    this.Enabled2 = true;
                    this.Cursor = Cursors.Hand;
                }));
            }
        }

        public string Text2
        {
            get
            {
                return this.label1.Text;
            }
            set
            {
                this.label1.Text = value;
                this.Update();
            }
        }

        public bool Enabled2
        {
            get { return this.enabled; }
            set {
                if(value)
                {
                    this.leftcorner.BackgroundImage = Properties.Resources.btn_left;
                    this.center.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
                    this.rightcorner.BackgroundImage = Properties.Resources.btnright;
                    this.Cursor = Cursors.Hand;
                    this.label1.ForeColor = Color.White;

                    this.leftcorner.Update();
                    this.center.Update();
                    this.rightcorner.Update();
                    this.label1.Update();
                } else
                {
                    this.leftcorner.BackgroundImage = Properties.Resources.btnleft_disabled;
                    this.center.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(112)))), ((int)(((byte)(160)))));
                    this.Cursor = Cursors.No;
                    this.rightcorner.BackgroundImage = Properties.Resources.btn_right_disabled;
                    this.label1.ForeColor = Color.LightGray;

                    this.leftcorner.Update();
                    this.center.Update();
                    this.rightcorner.Update();
                    this.label1.Update();

                }
                this.Update();
                this.enabled = value;
            }
        }

        bool isEnter = false;
        private void CustomButton_MouseEnter(object sender, EventArgs e)
        {
            if(isEnter || !this.Enabled2)
            {
                return;
            }
            isEnter = true;
            this.leftcorner.BackgroundImage = Properties.Resources.btnleft_disabled;
            this.center.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(112)))), ((int)(((byte)(160)))));
            this.rightcorner.BackgroundImage = Properties.Resources.btn_right_disabled;
            this.label1.ForeColor = Color.LightGray;
            //this.Update();
        }

        private void CustomButton_MouseLeave(object sender, EventArgs e)
        {
            if(!isEnter || !this.Enabled2)
            {
                return;
            }
            isEnter = false;
            this.leftcorner.BackgroundImage = Properties.Resources.btn_left;
            this.center.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.rightcorner.BackgroundImage = Properties.Resources.btnright;
            this.label1.ForeColor = Color.White;
            //this.Update();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            if(!this.Enabled2)
            {
                return;
            }
            this.PerformClick();
        }
    }
}
