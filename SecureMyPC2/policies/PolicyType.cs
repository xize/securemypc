﻿using SecureMyPC.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public class PolicyType
    {

        private static HashSet<PolicyType> types = new HashSet<PolicyType>();

        public static readonly PolicyType SRP_POLICY = new PolicyType("SRP_POLICY", new SRPPolicy(), true);
        public static readonly PolicyType DEFAULT_SCRIPT_POLICY = new PolicyType("DEFAULT_SCRIPT_POLICY", new DefaultScriptProgramPolicy());
        public static readonly PolicyType WSCRIPT_SCRIPT_POLICY = new PolicyType("WSCRIPT_SCRIPT_POLICY", new WscriptPolicy());
        public static readonly PolicyType UAC_POLICY = new PolicyType("UAC_POLICY", new UACPolicy());
        public static readonly PolicyType RDP_POLICY = new PolicyType("RDP_POLICY", new RDPPolicy());
        public static readonly PolicyType REMOTE_REGISTRY_POLICY = new PolicyType("REMOTE_REGISTRY_POLICY", new RemoteRegistryPolicy());
        public static readonly PolicyType ALWAYS_FORCE_ADMIN_PASSWORD_POLICY = new PolicyType("ALWAYS_FORCE_ADMIN_PASSWORD_POLICY", new AlwaysForceAdminPasswordPolicy());
        public static readonly PolicyType AUTORUN_POLICY = new PolicyType("AUTO_RUN_POLICY", new AutoRunPolicy());
        public static readonly PolicyType NETBIOS_POLICY = new PolicyType("NETBIOS_POLICY", new NetbiosPolicy());
        public static readonly PolicyType RECENT_DOCUMENT_POLICY = new PolicyType("RECENT_DOCUMENT_POLICY", new RecentDocumentPolicy());

        public static readonly OptionalPolicyType MBR_POLICY = new OptionalPolicyType("MBR_POLICY", new MBRFilterPolicy());
        public static readonly OptionalPolicyType PRINT_SPOOLER_POLICY = new OptionalPolicyType("PRINT_SPOOLER_POLICY", new PrintSpoolerPolicy());
        public static readonly OptionalPolicyType DUCK_HUNTER_POLICY = new OptionalPolicyType("DUCK_HUNTER_POLICY", new DuckHunterPolicy());
        public static readonly OptionalPolicyType RUN_AS_NON_ADMIN_CONTEXT_POLICY = new OptionalPolicyType("RUN_AS_NON_ADMIN_CONTEXT_POLICY", new RunAsNonAdminContextPolicy());

        private string name;
        private IPolicy policy;
        private bool secpol;

        /**
         * <summary>The constructor which also adds a static addr to #types</summary>
         * <param name="name">the name of the policy type</param>
         * <param name="policy">the IPolicy class</param>
         */
        public PolicyType(string name, IPolicy policy, bool secpol = false)
        {
            this.name = name;
            this.secpol = secpol;
            this.policy = policy;
            types.Add(this);
        }

        /**
         * <summary>returns the name of the policy type</summary>
         * <returns>string - the name</returns>
         */
        public string Name
        {
            get { return this.name; }
        }

        /**
         * <summary>returns the policy object</summary>
         * <return>IPolicy - the policy object</return>
         */
        public IPolicy Policy
        {
            get { return this.policy; }
        }

        /**
         * <summary>returns true if the policy is secpol depended</summary>
         * <returns>bool - true when this policy is secpol depended, false otherwise</returns>
         */
        public bool IsSecpol()
        {
            return this.secpol;
        }

        public static PolicyType ValueOf(string name)
        {
            foreach(PolicyType type in types)
            {
                if(type.name.ToUpper().StartsWith(name.ToUpper()))
                {
                    return type;
                }
            }
            throw new ArgumentException("no value found with name "+name.ToUpper());
        }

        public static PolicyType[] SecpolValues()
        {
            PolicyType[] policiess = new PolicyType[0];
            int index = -1;
            foreach(PolicyType p in Values())
            {
                if (p.IsSecpol())
                    policiess[index++] = p;
            }
            return policiess;
        }

        public static PolicyType[] Values()
        {
            return types.ToArray();
        }

    }
}
