﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public class OptionalPolicyType : PolicyType
    {
        public OptionalPolicyType(string name, IPolicy policy) : base(name, policy)
        {

        }

    }
}
