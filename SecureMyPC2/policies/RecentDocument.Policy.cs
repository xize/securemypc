﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public class RecentDocumentPolicy : IPolicy
    {

        public string GetName()
        {
            return "RecentDocumentPolicy";
        }


        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {

                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer", true);

                key.SetValue("NoRecentDocsHistory", 0x01, RegistryValueKind.DWord);

                key.Close();

                return true;
            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {

                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer", true);

                key.SetValue("NoRecentDocsHistory", 0x00, RegistryValueKind.DWord);

                key.Close();

                return true;
            });
            return t;
        }

        public bool IsEnabled()
        {
            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer");

            int v = (int)key.GetValue("NoRecentDocsHistory");

            key.Close();

            if(v == 0x01)
            {
                return true;
            }

            return false;
        }
    }
}
