﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public class NetbiosPolicy : IPolicy
    {

        public string GetName()
        {
            return "NetbiosPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SYSTEM\CurrentControlSet\Services\NetBT\Parameters\Interfaces", true);

                foreach(string subkey in key.GetSubKeyNames())
                {
                    RegistryKey s = key.OpenSubKey(subkey, true);

                    s.SetValue("NetbiosOptions", 0x02, RegistryValueKind.DWord);

                    s.Close();
                }

                key.Close();

                return true;
            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SYSTEM\CurrentControlSet\Services\NetBT\Parameters\Interfaces",true);

                foreach (string subkey in key.GetSubKeyNames())
                {
                    RegistryKey s = key.OpenSubKey(subkey, true);

                    s.SetValue("NetbiosOptions", 0x01, RegistryValueKind.DWord);

                    s.Close();
                }

                key.Close();

                return true;
            });
            return t;
        }

        public bool IsEnabled()
        {
            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SYSTEM\CurrentControlSet\Services\NetBT\Parameters\Interfaces");

            bool b = false;

            foreach (string subkey in key.GetSubKeyNames())
            {
                RegistryKey s = key.OpenSubKey(subkey);

                int v = (int)s.GetValue("NetbiosOptions");

                if(v == 0x02)
                {
                    return true;
                }

                s.Close();
            }
            key.Close();

            return b;
        }
    }
}
