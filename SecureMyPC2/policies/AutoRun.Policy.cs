﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public class AutoRunPolicy : IPolicy
    {

        public string GetName()
        {
            return "AutoRunPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer",true);

                key.SetValue("NoDriveTypeAutoRun", 0xFF, RegistryValueKind.DWord);

                key.Close();

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();

                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if(cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = true;
                } else
                {
                    cfg.Policies.Add(GetName(), true);
                }

                manager.SaveYamlConfig(cfg);

                return true;
            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer", true);

                key.SetValue("NoDriveTypeAutoRun", 0x00, RegistryValueKind.DWord);

                key.Close();

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();

                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if (cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = false;
                }
                else
                {
                    cfg.Policies.Add(GetName(), false);
                }

                manager.SaveYamlConfig(cfg);

                return true;
            });
            return t;
        }

        public bool IsEnabled()
        {
            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\Explorer");

            if (key.GetValue("NoDriveTypeAutoRun") != null && (int)key.GetValue("NoDriveTypeAutoRun") == 0xFF)
                return true;
            else
                return false;

        }
    }
}
