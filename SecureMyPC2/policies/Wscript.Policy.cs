﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC.policies
{
    public class WscriptPolicy : IPolicy
    {

        public string GetName()
        {
            return "WscriptPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() => {
                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows Script Host\Settings", true);
                key.SetValue("Enabled", 0, RegistryValueKind.String);
                key.Close();

                //save config.

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if(cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();
                

                if (!cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies.Add(GetName(), true);
                } else
                {
                    cfg.Policies[GetName()] = true;
                }

                manager.SaveYamlConfig(cfg);

                //end save config.

                return true;

            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() => {
                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows Script Host\Settings", true);
                key.SetValue("Enabled", 1, RegistryValueKind.String);
                key.Close();

                //save config.

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (!cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies.Add(GetName(), false);
                }
                else
                {
                    cfg.Policies[GetName()] = false;
                }

                manager.SaveYamlConfig(cfg);

                //end save config.

                return true;

            });
            return t;
        }

        public bool IsEnabled()
        {
            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows Script Host\Settings");
            try
            {
                if (key.GetValue("Enabled") != null && (int)key.GetValue("Enabled") == 0x0)
                {
                    key.Close();
                    return true;
                }
            } catch (Exception e) {
                key.Close();
                return false;
            }

            key.Close();
            return false;
        }
    }
}