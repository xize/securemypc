﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public interface IPolicy
    {

        string GetName();

        bool IsEnabled();

        Task<bool> Enable();

        Task<bool> Disable();

    }
}
