﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC.policies
{
    public class MBRFilterPolicy : IPolicy
    {

        public string GetName()
        {
            return "MBRFilterPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {
            DialogResult r = MessageBox.Show("MBRFilter is a third party program by Yves Younan, Cisco Talos\n\nbefore you install this beware that it might break secureboot\nsome programs will not work like win32disk imager\n\nAre you sure to continue Y/N?", "SecureMyPC: MBRFilter warning", MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk);

            if (r == DialogResult.No)
            {
                return false;
            }

            DialogResult r2 = MessageBox.Show("Do you want to read the LICENSE?", "SecureMyPC: MBRfilter", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);

            if (r2 == DialogResult.Yes)
            {
                Process.Start("explorer", "https://github.com/Cisco-Talos/MBRFilter/blob/master/LICENSE");
            } else if (r2 == DialogResult.Cancel)
            {
                return false;
            }

            bool is64 = Environment.Is64BitOperatingSystem;

            FileManager f = new FileManager();

            if (!Directory.Exists(f.DataFolder + "mbrfilter"))
            {
                Directory.CreateDirectory(f.DataFolder + "mbrfilter");
                Directory.CreateDirectory(f.DataFolder + @"mbrfilter\x64");
                Directory.CreateDirectory(f.DataFolder + @"mbrfilter\x86");
            }

            string mbrfilterpath = f.DataFolder + "mbrfilter" + Path.DirectorySeparatorChar;

            mbrfilterpath = Path.Combine(mbrfilterpath, (is64 ? "x64" : "x86") + Path.DirectorySeparatorChar);


            if (Directory.Exists(mbrfilterpath + Path.DirectorySeparatorChar + (is64 ? "64" : "32")))
                Directory.Delete(mbrfilterpath + Path.DirectorySeparatorChar + (is64 ? "64" : "32"), true);


                ZipFile.ExtractToDirectory("programs" + Path.DirectorySeparatorChar + "mbrfilter" + Path.DirectorySeparatorChar + (is64 ? "mbrfilter_x64.zip" : "mbrfilter_x86.zip"), mbrfilterpath);

                ProcessStartInfo proc = new ProcessStartInfo("cmd");
                proc.UseShellExecute = false;
                proc.CreateNoWindow = true;
                proc.Arguments = "/c InfDefaultInstall.exe "+mbrfilterpath+Path.DirectorySeparatorChar+(is64 ? "64":"32")+Path.DirectorySeparatorChar+"MBRFilter.inf";

                Process p = Process.Start(proc);

                p.WaitForExit();
                p.Close();

                Config cfg = f.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if(cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = true;
                } else
                {
                    cfg.Policies.Add(GetName(), true);
                }

                f.SaveYamlConfig(cfg);

                return true;
            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {
                this.Uninstall();

                FileManager f = new FileManager();
                Config cfg = f.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if(cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = false;
                } else
                {
                    cfg.Policies.Add(GetName(), false);
                }

                MessageBox.Show("note that you have to restart windows in order to make sure it has been removed.", "SecureMyPC: MBRFilter", MessageBoxButtons.OK, MessageBoxIcon.Information);

                return true;
            });
            return t;
        }

        private void Uninstall()
        {
            RegistryKey classes = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"System\CurrentControlSet\Control\Class\{4d36e967-e325-11ce-bfc1-08002be10318}\", true);
            List<String> orginal = new List<string>((string[])classes.GetValue("UpperFilters"));
            orginal.Remove("MBRFilter");
            classes.SetValue("UpperFilters", orginal.ToArray());

            classes.Close();
        }

        private bool IsInstalled()
        {
            try
            {
                RegistryKey classes = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"System\CurrentControlSet\Control\Class\{4d36e967-e325-11ce-bfc1-08002be10318}");
                List<String> data = new List<string>((String[])classes.GetValue("UpperFilters"));
                classes.Close();
                return data.Contains("MBRFilter");
            }
            catch (Exception) { return false; }
        }


        public bool IsEnabled()
        {
            FileManager manager = new FileManager();
            Config cfg = manager.LoadYamlConfig();
            if (cfg == null)
                cfg = new Config();

            if (cfg.Policies == null)
                cfg.Policies = new Dictionary<string, bool>();

            if(this.IsInstalled() || cfg.Policies.ContainsKey(GetName()) && cfg.Policies[GetName()])
            {
                return true;
            }

            return false;
        }
    }
}
