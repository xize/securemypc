﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public class RemoteRegistryPolicy : IPolicy
    {
        public string GetName()
        {
            return "RemoteRegistryPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {
                ProcessStartInfo pinfo1 = new ProcessStartInfo("net");
                pinfo1.UseShellExecute = false;
                pinfo1.CreateNoWindow = true;
                pinfo1.Arguments = "stop RemoteRegistry";

                ProcessStartInfo pinfo2 = new ProcessStartInfo("sc");
                pinfo2.UseShellExecute = false;
                pinfo2.CreateNoWindow = true;
                pinfo2.Arguments = "config RemoteRegistry start=disabled";

                Process p1 = Process.Start(pinfo1);
                Process p2 = Process.Start(pinfo2);

                p1.WaitForExit();
                p2.WaitForExit();

                p1.Close();
                p2.Close();

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();

                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if(cfg.Policies.ContainsKey(this.GetName()))
                {
                    cfg.Policies[GetName()] = true;
                } else
                {
                    cfg.Policies.Add(GetName(), true);
                }

                manager.SaveYamlConfig(cfg);

                return true;
            });

            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {
                //since its default off in win10, we only set remoteregistry on manual, and don't start it.


                ProcessStartInfo pinfo2 = new ProcessStartInfo("sc");
                pinfo2.UseShellExecute = false;
                pinfo2.CreateNoWindow = true;
                pinfo2.Arguments = "config RemoteRegistry start=manual";

                Process p2 = Process.Start(pinfo2);

                p2.WaitForExit();

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();

                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if (cfg.Policies.ContainsKey(this.GetName()))
                {
                    cfg.Policies[GetName()] = false;
                }
                else
                {
                    cfg.Policies.Add(GetName(), false);
                }

                manager.SaveYamlConfig(cfg);

                return true;
            });

            return t;
        }

        public bool IsEnabled()
        {
            FileManager manager = new FileManager();
            Config cfg = manager.LoadYamlConfig();

            if (cfg == null)
                return false;

            if (cfg.Policies == null)
                return false;

            if (!cfg.Policies.ContainsKey(GetName()))
                return false;

            return cfg.Policies[GetName()];
        }
    }
}
