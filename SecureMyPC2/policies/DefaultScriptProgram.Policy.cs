﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC.policies
{
    public class DefaultScriptProgramPolicy : IPolicy
    {
        #region extensions
        private string[] extensions =
        {
            //scripts, however there are alot more but I lost the article....
            ".js",
            //".ahk",
            ".svg",
            ".jse",
            ".vbs",
            ".wsh",
            ".wsc",
            ".wsf",
            ".vbx",
            ".vba",
            ".vb",
            ".vbe",
            ".ws",
            ".ocx",
            ".scr",
            ".pif",
            ".ps1", //since this is default set to notepad in alot of windows versions, older non updated versions still use ps1 as powershell application, for this reason keep patching it to notepad
            //macros need to be blocked aswell :)
            ".docm",
            ".dotm",
            ".pptm",
            ".xlm",
            ".xlsm",
            //html and help components
            ".mhtml",
            ".mht",
            ".hta",
            ".cfm",
            //special cases...
            ".pdf", //because it is vulnerable
            ".tif",
            ".tiff" //Tempory disable Tiff remote explotation, see: https://threatpost.com/remote-code-execution-vulnerabilities-plague-libtiff-library/121570/ not sure if this is the correct way of megitation!
        };
        #endregion


        public string GetName()
        {
            return "DefaultScriptProgramPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {
                StringBuilder b = new StringBuilder();
                foreach(string ext in this.extensions)
                {
                    b.Append("ftype " + ext.ToUpper().Substring(1) + "File=C:\\windows\\system32\\notepad.exe && ");
                }

                string d = b.ToString();
                int trim = d.LastIndexOf(' ')-3;

                d = d.Substring(0, trim);
                //MessageBox.Show("debug: "+ d);


                ExecuteCMD(d, true);

                FileManager manager = new FileManager();

                Config cfg = manager.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if(!cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies.Add(GetName(), true);
                } else
                {
                    cfg.Policies[GetName()] = true;
                }

                manager.SaveYamlConfig(cfg);
                
                return true;
            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> task = Task.Run(() => {

                BrowserType type = Browser.getBrowser().getCurrentBrowserType();

                string officepath = null;
                char[] disks = { 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

                foreach (char disk in disks)
                {
                    if (Directory.Exists(disk + @":\Program Files (x86)\Microsoft Office\root"))
                    {
                        officepath = Directory.EnumerateDirectories(disk + @":\Program Files (x86)\Microsoft Office\root\").First();
                    }
                }

                StringBuilder build = new StringBuilder();

                foreach (string extension in extensions)
                {
                    //ExecuteCMD("assoc " + extension + " = " + extension.ToUpper() + "File", true);

                    string argument = "";

                    switch (extension)
                    {
                        //TODO: figuring out how these macros work..... which program it uses to be exact.
                        //http://filext.com/file-extension/DOCM good site to check the default locations for. <- this list is not done yet!
                        case ".docm":
                            if (officepath != null)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + officepath + "\\winword.exe /n");
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + officepath + "\\winword.exe /n\" && ";
                            }
                            break;
                        case ".dotm":
                            if (officepath != null)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + officepath + "\\winword.exe /n");
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + officepath + "\\winword.exe /n\" && ";
                            }
                            break;
                        case ".pptm":
                            if (officepath != null)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + officepath + "\\powerpoint.exe");
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + officepath + "\\powerpoint.exe\" && ";
                            }
                            break;
                        case ".xlm":
                            if (officepath != null)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + officepath + "\\Excel.exe /e");
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + officepath + "\\Excel.exe /e\" && ";
                            }
                            break;
                        case ".xlsm":
                            if (officepath != null)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + officepath + "\\Excel.exe /e");
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + officepath + "\\Excel.exe /e\" && ";
                            }
                            break;
                        case ".ps1":
                            Console.WriteLine("extension: " + extension + " gets defaulted to: " + @"C:\windows\system32\notepad.exe");
                            argument = "ftype " + extension.Substring(1).ToUpper() + @"File=C:\Windows\System32\notepad.exe && ";
                            break;
                        case ".mhtml":
                            Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.INTERNET_EXPLORE.getPath() + " -nohome");
                            argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.INTERNET_EXPLORE.getPath() + " -nohome\" && ";
                            break;
                        case ".mht":
                            Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.INTERNET_EXPLORE.getPath() + " -nohome");
                            argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.INTERNET_EXPLORE.getPath() + " -nohome\" && ";
                            break;
                        case ".hta":
                            Console.WriteLine("extension: " + extension + @" gets defaulted to: C:\windows\SYSTEM32\mshta.exe %1");
                            argument = "ftype " + extension.Substring(1).ToUpper() + @"File=C:\windows\system32\mshta.exe %1 && ";
                            break;
                        case ".svg":
                            Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.INTERNET_EXPLORE.getPath() + " -nohome");
                            argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.INTERNET_EXPLORE.getPath() + " -nohome\" && ";
                            break;
                        case ".pdf":
                            if (type == BrowserType.CHROME)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.CHROME.getPath());
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.CHROME.getPath() + "\" && ";
                            }
                            else if (type == BrowserType.FIREFOX)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.FIREFOX.getPath());
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.FIREFOX.getPath() + "\" && ";
                            }
                            else
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.INTERNET_EXPLORE.getPath());
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.INTERNET_EXPLORE.getPath() + "\" && ";
                            }
                            break;
                        case ".cfm":
                            if (type == BrowserType.CHROME)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.CHROME.getPath());
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.CHROME.getPath() + "  %1\" && ";
                            }
                            else if (type == BrowserType.FIREFOX)
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.FIREFOX.getPath());
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.FIREFOX.getPath() + " %1\" && ";
                            }
                            else
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to: " + BrowserType.INTERNET_EXPLORE.getPath());
                                argument = "ftype " + extension.Substring(1).ToUpper() + "File=\"" + BrowserType.INTERNET_EXPLORE.getPath() + " %1\" && ";
                            }
                            break;
                        case ".ahk":
                            string path = Environment.Is64BitOperatingSystem ? @"C:\Program Files\AutoHotkey\AutoHotkeyU64.exe" : @"C:\Program Files (x86)\AutoHotkey\AutoHotkeyU32.exe";
                            //make sure it is installed atleast...
                            if (File.Exists(path))
                            {
                                Console.WriteLine("extension: " + extension + " gets defaulted to:" + path);
                                argument = "ftype AutoHotkeyScript=" + path + " %1 &&";
                            }
                            break;
                        default:
                            Console.WriteLine("extension: " + extension + " gets defaulted to: " + @"C:\Windows\System32\wscript.exe");
                            argument = "ftype " + extension.Substring(1).ToUpper() + @"File=C:\Windows\System32\wscript.exe %1 && ";
                            break;
                    }

                    build.Append(argument);
                }

                build.Append("echo done!");

                ExecuteCMD(build.ToString(), true);

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();

                if (cfg == null)
                    cfg = new Config();

                if(!cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies.Add(GetName(), false);
                } else
                {
                    cfg.Policies[GetName()] = false;
                }

                manager.SaveYamlConfig(cfg);

                return true;
            });

            return task;
        }

        public bool IsEnabled()
        {
            FileManager manager = new FileManager();
            Config cfg = manager.LoadYamlConfig();
            if (cfg == null)
                cfg = new Config();

            bool b = false;

            if (cfg.Policies == null)
                cfg.Policies = new Dictionary<string, bool>();

            if (cfg.Policies.ContainsKey(GetName()))
                b = cfg.Policies[GetName()];

            return b;
        }

        public void ExecuteCMD(string arguments, bool ghost)
        {
            ProcessStartInfo info = new ProcessStartInfo("cmd.exe");
            info.Arguments = "/c " + arguments;
            if (ghost)
            {
                info.UseShellExecute = false;
                info.CreateNoWindow = true;
            }
            Process p = Process.Start(info);
            while (!p.HasExited) { }
            p.Close();
            p.Dispose();
        }
    }
}
