﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public class UACPolicy : IPolicy
    {
        public string GetName()
        {
            return "UACPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey basekey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", true);

                //basekey.SetValue("ConsentPromptBehaviorAdmin", 0x02, RegistryValueKind.DWord);
                basekey.SetValue("ConsentPromptBehaviorUser", 0x03, RegistryValueKind.DWord);
                basekey.SetValue("EnableInstallerDetection", 0x01, RegistryValueKind.DWord);
                basekey.SetValue("EnableLUA", 0x01, RegistryValueKind.DWord);
                basekey.SetValue("EnableVirtualization", 0x01, RegistryValueKind.DWord);
                basekey.SetValue("PromptOnSecureDesktop", 0x01, RegistryValueKind.DWord);

                basekey.Close();

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if(cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = true;
                } else
                {
                    cfg.Policies.Add(GetName(), true);
                }

                manager.SaveYamlConfig(cfg);

                return true;
            });

            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey basekey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", true);

                //basekey.SetValue("ConsentPromptBehaviorAdmin", 0x05, RegistryValueKind.DWord);
                basekey.SetValue("ConsentPromptBehaviorUser", 0x03, RegistryValueKind.DWord);
                basekey.SetValue("EnableInstallerDetection", 0x01, RegistryValueKind.DWord);
                basekey.SetValue("EnableLUA", 0x01, RegistryValueKind.DWord);
                basekey.SetValue("EnableVirtualization", 0x01, RegistryValueKind.DWord);
                basekey.SetValue("PromptOnSecureDesktop", 0x00, RegistryValueKind.DWord);

                basekey.Close();

                FileManager manager = new FileManager();
                Config cfg = manager.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if (cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = false;
                }
                else
                {
                    cfg.Policies.Add(GetName(), false);
                }

                manager.SaveYamlConfig(cfg);

                return true;
            });

            return t;
        }

        public bool IsEnabled()
        {
            FileManager manager = new FileManager();
            Config cfg = manager.LoadYamlConfig();

            RegistryKey basekey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System", true);

            if((int)basekey.GetValue("PromptOnSecureDesktop") == 0x01 || cfg != null && cfg.Policies != null && cfg.Policies.ContainsKey(GetName()) && cfg.Policies[GetName()])
            {
                return true;
            }


            return false;            
        }
    }
}
