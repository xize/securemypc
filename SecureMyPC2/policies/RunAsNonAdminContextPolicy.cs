﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC.policies
{
    public class RunAsNonAdminContextPolicy : IPolicy
    {
        public string GetName()
        {
            return "RunAsNonAdminContextPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey root = RegistryKey.OpenBaseKey(RegistryHive.ClassesRoot, RegistryView.Default).OpenSubKey(@"*\shell", true);
                RegistryKey progkey = root.CreateSubKey("forcenonadministrator");
                progkey.SetValue("", "Force run as non administrator");
                RegistryKey command = progkey.CreateSubKey("command");
                command.SetValue("", "cmd.exe /c set __COMPAT_LAYER=RUNASINVOKER && \"%1\"");

                command.Close();
                progkey.Close();
                root.Close();

                return true;
            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey root = RegistryKey.OpenBaseKey(RegistryHive.ClassesRoot, RegistryView.Default).OpenSubKey(@"*\shell", true);
                root.DeleteSubKeyTree("forcenonadministrator");

                root.Close();

                return true;
            });
            return t;
        }

        public bool IsEnabled()
        {
            RegistryKey root = RegistryKey.OpenBaseKey(RegistryHive.ClassesRoot, RegistryView.Default).OpenSubKey(@"*\shell", true);
            RegistryKey k = root.OpenSubKey("forcenonadministrator");
            if(k != null)
            {
                k.Close();
                root.Close();
                return true;
            }
            root.Close();
            return false;
        }
    }
}
