﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC.policies
{
    public class DuckHunterPolicy : IPolicy
    {
        public string GetName()
        {
            return "DuckHunterPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                key.SetValue("DuckHunter", "\""+Application.StartupPath + Path.DirectorySeparatorChar+"programs"+Path.DirectorySeparatorChar+"duckhunter"+Path.DirectorySeparatorChar+"duckhunt.0.9.exe" + "\"");
                key.Close();

                FileManager f = new FileManager();
                Config cfg = f.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if(cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = true;
                } else
                {
                    cfg.Policies.Add(GetName(), true);
                }

                f.SaveYamlConfig(cfg);

                ProcessStartInfo pinfo = new ProcessStartInfo("cmd");
                pinfo.UseShellExecute = false;
                pinfo.CreateNoWindow = true;
                pinfo.Arguments = "/c " + Application.StartupPath + Path.DirectorySeparatorChar + "programs" + Path.DirectorySeparatorChar + "duckhunter" + Path.DirectorySeparatorChar + "duckhunt.0.9.exe";
                
                Process p = Process.Start(pinfo);

                Task t2 = Task.Delay(1000);
                t2.Wait();

                p.Close();

                return true;
            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {
                RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
                key.DeleteValue("DuckHunter");
                key.Close();

                FileManager f = new FileManager();
                Config cfg = f.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if (cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = false;
                }
                else
                {
                    cfg.Policies.Add(GetName(), false);
                }

                f.SaveYamlConfig(cfg);

                Process[] procs = Process.GetProcessesByName("duckhunt.0.9");

                if(procs.Length > 0)
                {
                    foreach(Process proc in procs)
                    {
                        proc.Kill();
                    }
                }

                return true;
            });
            return t;
        }

        public bool IsEnabled()
        {
            RegistryKey key = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Default).OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);

            object v = key.GetValue("DuckHunter");

            key.Close();

            if(v != null)
            {
                return true;
            }
            return false;
        }
    }
}
