﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SecureMyPC.policies
{
    public class PrintSpoolerPolicy : IPolicy
    {
        public string GetName()
        {
            return "PrintSpoolerPolicy";
        }

        public Task<bool> Enable()
        {
            Task<bool> t = Task.Run(() =>
            {

                ProcessStartInfo pinfo = new ProcessStartInfo("net");
                pinfo.UseShellExecute = false;
                pinfo.CreateNoWindow = true;
                pinfo.Arguments = "stop Spooler";

                ProcessStartInfo pinfo2 = new ProcessStartInfo("sc");
                pinfo2.UseShellExecute = false;
                pinfo2.CreateNoWindow = true;
                pinfo2.Arguments = "config Spooler start=disabled";

                Process p1 = Process.Start(pinfo);
                Process p2 = Process.Start(pinfo2);

                p1.WaitForExit();
                p2.WaitForExit();

                p1.Close();
                p2.Close();

                FileManager f = new FileManager();
                Config cfg = f.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if(cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = true;
                } else
                {
                    cfg.Policies.Add(GetName(), true);
                }

                f.SaveYamlConfig(cfg);

                return true;
            });
            return t;
        }

        public Task<bool> Disable()
        {
            Task<bool> t = Task.Run(() =>
            {
                ProcessStartInfo pinfo = new ProcessStartInfo("net");
                pinfo.UseShellExecute = false;
                pinfo.CreateNoWindow = true;
                pinfo.Arguments = "start Spooler";

                ProcessStartInfo pinfo2 = new ProcessStartInfo("sc");
                pinfo2.UseShellExecute = false;
                pinfo2.CreateNoWindow = true;
                pinfo2.Arguments = "config Spooler start=automatic";

                Process p1 = Process.Start(pinfo);
                Process p2 = Process.Start(pinfo2);

                p1.WaitForExit();
                p2.WaitForExit();

                FileManager f = new FileManager();
                Config cfg = f.LoadYamlConfig();
                if (cfg == null)
                    cfg = new Config();

                if (cfg.Policies == null)
                    cfg.Policies = new Dictionary<string, bool>();

                if (cfg.Policies.ContainsKey(GetName()))
                {
                    cfg.Policies[GetName()] = false;
                }
                else
                {
                    cfg.Policies.Add(GetName(), false);
                }

                f.SaveYamlConfig(cfg);

                return true;
            });
            return t;
        }

        public bool IsEnabled()
        {
            FileManager f = new FileManager();
            Config cfg = f.LoadYamlConfig();
            if (cfg == null)
                return false;

            if (cfg.Policies == null)
                return false;

            if (!cfg.Policies.ContainsKey(GetName()))
                return false;
            
            return cfg.Policies[GetName()];
        }
    }
}
