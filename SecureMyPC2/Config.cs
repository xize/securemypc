﻿using SecureMyPC.policies;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC
{
    public class Config
    {
        public string Version { get; set; }

        public Dictionary<string, bool> Policies { get; set; }

        public Dictionary<string, string> WORKDIR_LOCATIONS { get; set; }

    }
}
