﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using YamlDotNet.Serialization;
using YamlDotNet.Serialization.NamingConventions;

namespace SecureMyPC
{
    public class FileManager
    {

        public string DataFolder
        {
            get
            {
                string appdata = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
                string dt = appdata + Path.DirectorySeparatorChar + "SecureMyPC"+Path.DirectorySeparatorChar;

                if (!Directory.Exists(dt))
                {
                    Directory.CreateDirectory(dt);
                }

                return dt;

            }
        }


        public Config LoadYamlConfig()
        {
            if(!File.Exists(DataFolder + "config.yml"))
            {
                return null;
            }
            var deserializer = new DeserializerBuilder()//.WithNamingConvention(UnderscoredNamingConvention.Instance)
           .Build();


            string yaml = File.ReadAllText(DataFolder + "config.yml");

            Config cfg = deserializer.Deserialize<Config>(yaml);
            
            
            return cfg;
        }

        public void SaveYamlConfig(Config cfg)
        {
            cfg.Version = Application.ProductVersion;
            var serializer = new SerializerBuilder()//.WithNamingConvention(CamelCaseNamingConvention.Instance)
            .Build();
            var yaml = serializer.Serialize(cfg);

            File.WriteAllText(DataFolder + "config.yml", yaml);
        }

    }
}
