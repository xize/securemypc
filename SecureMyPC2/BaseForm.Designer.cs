﻿
namespace SecureMyPC
{
    partial class BaseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BaseForm));
            this.logo = new System.Windows.Forms.Panel();
            this.progressControl1 = new SecureMyPC.pages.ProgressControl();
            this.mainControl1 = new SecureMyPC.MainControl();
            this.customButton2 = new SecureMyPC.controls.CustomButton();
            this.customButton1 = new SecureMyPC.controls.CustomButton();
            this.customButton23 = new SecureMyPC.controls.CustomButton();
            this.optionalControl1 = new SecureMyPC.OptionalControl();
            this.roundedControl1 = new SecureMyPC.controls.SecureMyPC.controls.RoundedControl();
            this.moveAbleWindow1 = new SecureMyPC.controls.SecureMyPC.controls.MoveAbleWindow();
            this.SuspendLayout();
            // 
            // logo
            // 
            this.logo.BackgroundImage = global::SecureMyPC.Properties.Resources.logo;
            this.logo.Location = new System.Drawing.Point(12, 10);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(214, 47);
            this.logo.TabIndex = 1;
            // 
            // progressControl1
            // 
            this.progressControl1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.progressControl1.BackColor = System.Drawing.SystemColors.Control;
            this.progressControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.progressControl1.ForeColor = System.Drawing.Color.DimGray;
            this.progressControl1.IgnoreSetProgress = false;
            this.progressControl1.IsProgressing = false;
            this.progressControl1.Location = new System.Drawing.Point(12, 570);
            this.progressControl1.Name = "progressControl1";
            this.progressControl1.Size = new System.Drawing.Size(399, 60);
            this.progressControl1.TabIndex = 56;
            this.progressControl1.TargetControl = this.mainControl1;
            // 
            // mainControl1
            // 
            this.mainControl1.Location = new System.Drawing.Point(0, 87);
            this.mainControl1.Name = "mainControl1";
            this.mainControl1.ProgressTarget = this.progressControl1;
            this.mainControl1.Size = new System.Drawing.Size(424, 485);
            this.mainControl1.TabIndex = 55;
            // 
            // customButton2
            // 
            this.customButton2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.customButton2.BackColor = System.Drawing.Color.Transparent;
            this.customButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customButton2.Enabled2 = true;
            this.customButton2.Location = new System.Drawing.Point(304, 65);
            this.customButton2.Name = "customButton2";
            this.customButton2.Size = new System.Drawing.Size(108, 17);
            this.customButton2.TabIndex = 54;
            this.customButton2.Text2 = "Optional Options";
            this.customButton2.ButtonClick += new System.EventHandler(this.customButton2_ButtonClick);
            // 
            // customButton1
            // 
            this.customButton1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.customButton1.BackColor = System.Drawing.Color.Transparent;
            this.customButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customButton1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customButton1.Enabled2 = true;
            this.customButton1.Location = new System.Drawing.Point(198, 65);
            this.customButton1.Name = "customButton1";
            this.customButton1.Size = new System.Drawing.Size(100, 17);
            this.customButton1.TabIndex = 53;
            this.customButton1.Text2 = "Main options";
            this.customButton1.ButtonClick += new System.EventHandler(this.customButton1_ButtonClick);
            // 
            // customButton23
            // 
            this.customButton23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.customButton23.BackColor = System.Drawing.Color.Transparent;
            this.customButton23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.customButton23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.customButton23.Enabled2 = true;
            this.customButton23.Location = new System.Drawing.Point(358, 7);
            this.customButton23.Name = "customButton23";
            this.customButton23.Size = new System.Drawing.Size(54, 17);
            this.customButton23.TabIndex = 48;
            this.customButton23.Text2 = "Close";
            this.customButton23.ButtonClick += new System.EventHandler(this.customButton23_ButtonClick);
            // 
            // optionalControl1
            // 
            this.optionalControl1.Location = new System.Drawing.Point(0, 88);
            this.optionalControl1.Name = "optionalControl1";
            this.optionalControl1.ProgressTarget = null;
            this.optionalControl1.Size = new System.Drawing.Size(424, 225);
            this.optionalControl1.TabIndex = 57;
            this.optionalControl1.Visible = false;
            // 
            // roundedControl1
            // 
            this.roundedControl1.Border = 0;
            this.roundedControl1.BorderColor = System.Drawing.Color.Transparent;
            this.roundedControl1.Radius = 30;
            this.roundedControl1.TargetControl = this;
            // 
            // moveAbleWindow1
            // 
            this.moveAbleWindow1.ControlTarget = null;
            this.moveAbleWindow1.MainWindowTarget = this;
            // 
            // BaseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(424, 642);
            this.Controls.Add(this.progressControl1);
            this.Controls.Add(this.mainControl1);
            this.Controls.Add(this.customButton2);
            this.Controls.Add(this.customButton1);
            this.Controls.Add(this.customButton23);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.optionalControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "BaseForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SecureMyPc ®";
            this.Load += new System.EventHandler(this.Window_Load);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel logo;
        private controls.SecureMyPC.controls.RoundedControl roundedControl1;
        private controls.CustomButton customButton23;
        private controls.SecureMyPC.controls.MoveAbleWindow moveAbleWindow1;
        private controls.CustomButton customButton2;
        private controls.CustomButton customButton1;
        private MainControl mainControl1;
        private pages.ProgressControl progressControl1;
        private OptionalControl optionalControl1;
    }
}

