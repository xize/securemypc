﻿using SecureMyPC.policies;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace SecureMyPC
{
    public class LGPO
    {

        private IPolicy p;

        /**
         * <summary>The LGPO wrapper constructor</summary>
         * <param name="p">the policy for workspace naming</param>
         */
        public LGPO(IPolicy p)
        {
            this.p = p;
        }

        /**
         * <summary>The script which get set in the builder</summary>
         */
        private string Script
        {
            get;set;
        }

        /**
         * <summary>returns true if the parsing had a error</summary>
         */
        public bool IsError
        {
            get;private set;
        }

        /**
         * <summary>this is the root directory of the workspace, each folder represents its own policy</summary>
         */
        public string RootSpace { 
            get
            {
                string workspace_root = @"C:\SMP_WORKSPACE\";

                if (!Directory.Exists(workspace_root))
                {
                    Directory.CreateDirectory(workspace_root);
                    File.WriteAllBytes(workspace_root + "lgpo.exe", Properties.Resources.LGPO);
                }
                return workspace_root;
            }
        }

        /**
         * <summary>this is the workspace, the workspace is for its own script as environment</summary>
         */
        private string Workspace
        {   get
            {

                if(!Directory.Exists(RootSpace + "backup"))
                {
                    Directory.CreateDirectory(RootSpace + "backup");
                    File.Copy(@"C:\Windows\Sysnative\GroupPolicy\Machine\Registry.pol", RootSpace + "backup"+Path.DirectorySeparatorChar+"Registry.pol", true);
                }

                string wrk = RootSpace + p.GetName()+Path.DirectorySeparatorChar;

                if (!Directory.Exists(wrk))
                    Directory.CreateDirectory(wrk);

                return wrk;
            }
        }

        /**
         * <summary>this does not use lgpo yet.\nhowever this adds the script and its header and footer</summary>
         */
        public LGPO BuildScript(byte[] script)
        {
            CleanUp();

            Script = Encoding.UTF8.GetString(script);
            return this;
        }

        public LGPO BuildUnApplyScript(byte[] unapply)
        {
            CleanUp();

            Script = Encoding.UTF8.GetString(unapply);
            return this;
        }

        /**
         * <summary>this parses the script which got created by #<see cref="BuildScript(string)"/>
         * and writes it as a Registry.pol</summary>
         */
        public LGPO CompileScriptToRegistryPol()
        {

            ProcessStartInfo pinfo1 = new ProcessStartInfo("cmd");
            pinfo1.Arguments = "/c " + RootSpace + @"lgpo.exe /parse /m C:\Windows\Sysnative\GroupPolicy\Machine\Registry.pol > "+this.Workspace+"registry.script";
            pinfo1.UseShellExecute = false;
            pinfo1.CreateNoWindow = true;
            pinfo1.RedirectStandardInput = true;
            Process p1 = Process.Start(pinfo1);

            p1.WaitForExit();

            string registryscripttext = File.ReadAllText(this.Workspace+"registry.script");
            registryscripttext.Replace("; PARSING COMPLETED.\n", "");
            registryscripttext.Replace(";----------------------------------------------------------------------\n","");
            registryscripttext += Script+"\n\n";
            registryscripttext += "; PARSING COMPLETED.\n;----------------------------------------------------------------------\n";
            File.WriteAllText(this.Workspace + "registry.script", registryscripttext);


            ProcessStartInfo pinfo2 = new ProcessStartInfo("cmd");
            pinfo2.Arguments = "/c "+RootSpace+"lgpo.exe /r "+Workspace+"registry.script /w "+Workspace+"Registry.pol";
            pinfo2.UseShellExecute = false;
            pinfo2.CreateNoWindow = true;
            pinfo2.RedirectStandardInput = true;

            Process p2 = Process.Start(pinfo2);
            p2.WaitForExit();
            
            /*StreamReader reader = p2.StandardOutput;
            string text = reader.ReadToEnd();

            if (text.Contains("Cannot") || text.ToLower().Contains("error"))
                IsError = true;
            else
                IsError = false;
            */
            return this;
        }

        /**
         * <summary>once the script has been compiled by <see cref="CompileScriptToRegistryPol()"/> the compiled Registry.pol gets copied to C:\windows\Sysnative\GroupManager\Machine\Registry.pol</summary>
         */
        public bool Apply()
        {
            if (IsError)
                return false;

            try
            {
                File.Copy(Workspace + "Registry.pol", @"C:\windows\Sysnative\GroupManager\Machine\Registry.pol", true);
            } catch(Exception)
            {
                return false;
            }
            return true;
        }

        public bool UnApply()
        {
            IsError = false;

            ProcessStartInfo pinfo1 = new ProcessStartInfo("cmd");
            pinfo1.Arguments = "/c " + RootSpace + @"lgpo.exe /parse /m C:\Windows\Sysnative\GroupPolicy\Machine\Registry.pol > " + this.Workspace + "registry.script";
            pinfo1.UseShellExecute = false;
            pinfo1.CreateNoWindow = true;
            pinfo1.RedirectStandardInput = true;
            Process p1 = Process.Start(pinfo1);

            p1.WaitForExit();

            string registryscripttext = File.ReadAllText(this.Workspace + "registry.script");
            registryscripttext.Replace(Script, "");
            File.WriteAllText(this.Workspace + "registry.script", registryscripttext);


            ProcessStartInfo pinfo2 = new ProcessStartInfo("cmd");
            pinfo2.Arguments = "/c " + RootSpace + "lgpo.exe /r " + Workspace + "registry.script /w " + Workspace + "Registry.pol";
            pinfo2.UseShellExecute = false;
            pinfo2.CreateNoWindow = true;
            pinfo2.RedirectStandardInput = true;

            Process p2 = Process.Start(pinfo2);
            /*
            p2.BeginOutputReadLine();
            StreamReader reader = p2.StandardOutput;
            string text = reader.ReadToEnd();

            if (text.Contains("Cannot") || text.ToLower().Contains("error"))
                IsError = true;
            else
                IsError = false;
            */

            File.Copy(Workspace+"Registry.pol", @"C:\windows\Sysnative\GroupPolicy\Machine\Registry.pol", true);

            return !IsError;
        }

        /**
         * <summary>cleans up the work directory only for this policy</summary>
         */
        private void CleanUp()
        {
            string dir = Workspace;

            List<string> dirs = Directory.EnumerateDirectories(dir).ToList();
            List<string> files = Directory.EnumerateFiles(dir).ToList();

            if(dirs.Count > 0)
            {
                foreach(string d in dirs)
                {
                    Directory.Delete(d, true);
                }
            }

            if(files.Count > 0)
            {
                foreach(string f in files)
                {
                    File.Delete(f);
                }
            }
        }

    }
}
