﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LGPOArrayMaker
{
    public partial class Window : Form
    {
        public Window()
        {
            InitializeComponent();
        }

        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(this.richTextBox1.Text);

            int index = 0;
            StringBuilder builder = new StringBuilder();
            foreach(byte b in bytes)
            {
                if (index == 50)
                {
                    index = 0;
                    builder.Append(b+",\n");
                } else
                {
                    builder.Append(b + ",");
                }
                    
                index++;
            }

            richTextBox2.Text = builder.ToString();
        }

        private void richTextBox1_Click(object sender, EventArgs e)
        {
            this.richTextBox1.Clear();
        }

        private void richTextBox2_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.richTextBox2.Text);
            MessageBox.Show("the data has been added to the clipboard!", "LGPO Array Maker", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}
